//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.mvel2.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import org.mvel2.CompileException;
import org.mvel2.DataConversion;
import org.mvel2.MVEL;
import org.mvel2.OptimizationFailure;
import org.mvel2.ParserContext;
import org.mvel2.ast.ASTNode;
import org.mvel2.compiler.AbstractParser;
import org.mvel2.compiler.BlankLiteral;
import org.mvel2.compiler.CompiledExpression;
import org.mvel2.compiler.ExecutableAccessor;
import org.mvel2.compiler.ExecutableAccessorSafe;
import org.mvel2.compiler.ExecutableLiteral;
import org.mvel2.compiler.ExpressionCompiler;
import org.mvel2.integration.ResolverTools;
import org.mvel2.integration.VariableResolverFactory;
import org.mvel2.integration.impl.ClassImportResolverFactory;
import org.mvel2.math.MathProcessor;
import org.mvel2.util.NullType;
import org.mvel2.util.StringAppender;

public class ParseTools {
    public static final String[] EMPTY_STR_ARR = new String[0];
    public static final Object[] EMPTY_OBJ_ARR = new Object[0];
    public static final Class[] EMPTY_CLS_ARR = new Class[0];
    private static Map<String, Map<Integer, WeakReference<Method>>> RESOLVED_METH_CACHE;
    private static Map<Class, Map<Integer, WeakReference<Constructor>>> RESOLVED_CONST_CACHE;
    private static Map<Constructor, WeakReference<Class[]>> CONSTRUCTOR_PARMS_CACHE;
    private static Map<ClassLoader, Map<String, WeakReference<Class>>> CLASS_RESOLVER_CACHE;
    private static Map<Class, WeakReference<Constructor[]>> CLASS_CONSTRUCTOR_CACHE;
    private static final HashMap<Class, Integer> typeResolveMap;
    private static final Map<Class, Integer> typeCodes;

    public ParseTools() {
    }

    public static List<char[]> parseMethodOrConstructor(char[] parm) {
        int start = -1;

        for(int i = 0; i < parm.length; ++i) {
            if(parm[i] == 40) {
                ++i;
                start = i;
                break;
            }
        }

        if(start != -1) {
            --start;
            return parseParameterList(parm, start + 1, balancedCapture(parm, start, '(') - start - 1);
        } else {
            return Collections.emptyList();
        }
    }

    public static String[] parseParameterDefList(char[] parm, int offset, int length) {
        LinkedList list = new LinkedList();
        if(length == -1) {
            length = parm.length;
        }

        int start = offset;
        int i = offset;

        String s;
        for(int end = offset + length; i < end; ++i) {
            switch(parm[i]) {
                case '\"':
                    i = captureStringLiteral('\"', parm, i, parm.length);
                    break;
                case '\'':
                    i = captureStringLiteral('\'', parm, i, parm.length);
                    break;
                case '(':
                case '[':
                case '{':
                    i = balancedCapture(parm, i, parm[i]);
                    break;
                case ',':
                    if(i > start) {
                        while(isWhitespace(parm[start])) {
                            ++start;
                        }

                        checkNameSafety(s = new String(parm, start, i - start));
                        list.add(s);
                    }

                    while(isWhitespace(parm[i])) {
                        ++i;
                    }

                    start = i + 1;
                    break;
                default:
                    if(!isWhitespace(parm[i]) && !isIdentifierPart(parm[i])) {
                        throw new CompileException("expected parameter", parm, start);
                    }
            }
        }

        if(start < length + offset && i > start) {
            if((s = createStringTrimmed(parm, start, i - start)).length() > 0) {
                checkNameSafety(s);
                list.add(s);
            }
        } else if(list.size() == 0 && (s = createStringTrimmed(parm, start, length)).length() > 0) {
            checkNameSafety(s);
            list.add(s);
        }

        return (String[])list.toArray(new String[list.size()]);
    }

    public static List<char[]> parseParameterList(char[] parm, int offset, int length) {
        ArrayList list = new ArrayList();
        if(length == -1) {
            length = parm.length;
        }

        int start = offset;
        int i = offset;

        for(int end = offset + length; i < end; ++i) {
            switch(parm[i]) {
                case '\"':
                    i = captureStringLiteral('\"', parm, i, parm.length);
                    break;
                case '\'':
                    i = captureStringLiteral('\'', parm, i, parm.length);
                    break;
                case '(':
                case '[':
                case '{':
                    i = balancedCapture(parm, i, parm[i]);
                    break;
                case ',':
                    if(i > start) {
                        while(isWhitespace(parm[start])) {
                            ++start;
                        }

                        list.add(subsetTrimmed(parm, start, i - start));
                    }

                    while(isWhitespace(parm[i])) {
                        ++i;
                    }

                    start = i + 1;
            }
        }

        char[] s;
        if(start < length + offset && i > start) {
            s = subsetTrimmed(parm, start, i - start);
            if(s.length > 0) {
                list.add(s);
            }
        } else if(list.size() == 0) {
            s = subsetTrimmed(parm, start, length);
            if(s.length > 0) {
                list.add(s);
            }
        }

        return list;
    }

    public static Method getBestCandidate(Object[] arguments, String method, Class decl, Method[] methods, boolean requireExact) {
        Class[] targetParms = new Class[arguments.length];

        for(int i = 0; i != arguments.length; ++i) {
            targetParms[i] = arguments[i] != null?arguments[i].getClass():null;
        }

        return getBestCandidate(targetParms, method, decl, methods, requireExact);
    }

    public static Method getBestCandidate(Class[] arguments, String method, Class decl, Method[] methods, boolean requireExact) {
        return getBestCandidate(arguments, method, decl, methods, requireExact, false);
    }

    public static Method getBestCandidate(Class[] arguments, String method, Class decl, Method[] methods, boolean requireExact, boolean classTarget) {
        if(methods.length == 0) {
            return null;
        } else {
            Method bestCandidate = null;
            int bestScore = 0;
            int score = 0;
            boolean retry = false;
            Integer hash = Integer.valueOf(createClassSignatureHash(decl, arguments));
            Object methCache = (Map)RESOLVED_METH_CACHE.get(method);
            WeakReference ref;
            if(methCache != null && (ref = (WeakReference)((Map)methCache).get(hash)) != null && (bestCandidate = (Method)ref.get()) != null) {
                return bestCandidate;
            } else {
                while(true) {
                    Method[] objMethods = methods;
                    int nMethods = methods.length;

                    int i;
                    for(i = 0; i < nMethods; ++i) {
                        Method meth = objMethods[i];
                        if((!classTarget || (meth.getModifiers() & 8) != 0) && method.equals(meth.getName())) {
                            boolean isVarArgs = meth.isVarArgs();
                            Class[] parmTypes;
                            if((parmTypes = meth.getParameterTypes()).length == arguments.length || isVarArgs) {
                                if(arguments.length == 0 && parmTypes.length == 0) {
                                    bestCandidate = meth;
                                    break;
                                }

                                for(int i1 = 0; i1 != arguments.length; ++i1) {
                                    Class actualParamType;
                                    if(isVarArgs && i1 >= parmTypes.length - 1) {
                                        actualParamType = parmTypes[parmTypes.length - 1].getComponentType();
                                    } else {
                                        actualParamType = parmTypes[i1];
                                    }

                                    if(arguments[i1] == null) {
                                        if(actualParamType.isPrimitive()) {
                                            score = 0;
                                            break;
                                        }

                                        score += 5;
                                    } else if(actualParamType == arguments[i1]) {
                                        score += 6;
                                    } else if(actualParamType.isPrimitive() && boxPrimitive(actualParamType) == arguments[i1]) {
                                        score += 5;
                                    } else if(arguments[i1].isPrimitive() && unboxPrimitive(arguments[i1]) == actualParamType) {
                                        score += 5;
                                    } else if(isNumericallyCoercible(arguments[i1], actualParamType)) {
                                        score += 4;
                                    } else if(boxPrimitive(actualParamType).isAssignableFrom(boxPrimitive(arguments[i1])) && Object.class != arguments[i1]) {
                                        score += 3 + scoreInterface(actualParamType, arguments[i1]);
                                    } else if(!requireExact && DataConversion.canConvert(actualParamType, arguments[i1])) {
                                        if(actualParamType.isArray() && arguments[i1].isArray()) {
                                            ++score;
                                        } else if(actualParamType == Character.TYPE && arguments[i1] == String.class) {
                                            ++score;
                                        }

                                        ++score;
                                    } else {
                                        if(actualParamType != Object.class && arguments[i1] != NullType.class) {
                                            score = 0;
                                            break;
                                        }

                                        ++score;
                                    }
                                }

                                if(score != 0 && score > bestScore) {
                                    bestCandidate = meth;
                                    bestScore = score;
                                }

                                score = 0;
                            }
                        }
                    }

                    if(bestCandidate != null) {
                        if(methCache == null) {
                            RESOLVED_METH_CACHE.put(method, (Map<Integer, WeakReference<Method>>) (methCache = new WeakHashMap()));
                        }

                        ((Map)methCache).put(hash, new WeakReference(bestCandidate));
                        break;
                    }

                    if(retry || bestCandidate != null || !decl.isInterface()) {
                        break;
                    }

                    objMethods = Object.class.getMethods();
                    Method[] var21 = new Method[methods.length + objMethods.length];

                    for(i = 0; i < methods.length; ++i) {
                        var21[i] = methods[i];
                    }

                    for(i = 0; i < objMethods.length; ++i) {
                        var21[i + methods.length] = objMethods[i];
                    }

                    methods = var21;
                    retry = true;
                }

                return bestCandidate;
            }
        }
    }

    public static int scoreInterface(Class parm, Class arg) {
        if(parm.isInterface()) {
            Class[] iface = arg.getInterfaces();
            if(iface != null) {
                Class[] arr$ = iface;
                int len$ = iface.length;

                for(int i$ = 0; i$ < len$; ++i$) {
                    Class c = arr$[i$];
                    if(c == parm) {
                        return 1;
                    }

                    if(parm.isAssignableFrom(c)) {
                        return scoreInterface(parm, arg.getSuperclass());
                    }
                }
            }
        }

        return 0;
    }

    public static Method getExactMatch(String name, Class[] args, Class returnType, Class cls) {
        Method[] arr$ = cls.getMethods();
        int len$ = arr$.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            Method meth = arr$[i$];
            if(name.equals(meth.getName()) && returnType == meth.getReturnType()) {
                Class[] parameterTypes = meth.getParameterTypes();
                if(parameterTypes.length == args.length) {
                    for(int i = 0; i < parameterTypes.length; ++i) {
                        if(parameterTypes[i] != args[i]) {
                            return null;
                        }
                    }

                    return meth;
                }
            }
        }

        return null;
    }

    public static Method getWidenedTarget(Method method) {
        Class cls = method.getDeclaringClass();
        Method best = method;
        Class[] args = method.getParameterTypes();
        String name = method.getName();
        Class rt = method.getReturnType();

        do {
            Method m;
            if(cls.getInterfaces().length != 0) {
                Class[] arr$ = cls.getInterfaces();
                int len$ = arr$.length;

                for(int i$ = 0; i$ < len$; ++i$) {
                    Class iface = arr$[i$];
                    if((m = getExactMatch(name, args, rt, iface)) != null) {
                        best = m;
                        if(m.getDeclaringClass().getSuperclass() != null) {
                            cls = m.getDeclaringClass();
                        }
                    }
                }
            }

            if(cls != method.getDeclaringClass() && (m = getExactMatch(name, args, rt, cls)) != null) {
                best = m;
                if(m.getDeclaringClass().getSuperclass() != null) {
                    cls = m.getDeclaringClass();
                }
            }
        } while((cls = cls.getSuperclass()) != null);

        return best;
    }

    private static Class[] getConstructors(Constructor cns) {
        WeakReference ref = (WeakReference)CONSTRUCTOR_PARMS_CACHE.get(cns);
        Class[] parms;
        if(ref != null && (parms = (Class[])ref.get()) != null) {
            return parms;
        } else {
            CONSTRUCTOR_PARMS_CACHE.put(cns, new WeakReference(parms = cns.getParameterTypes()));
            return parms;
        }
    }

    public static Constructor getBestConstructorCandidate(Object[] args, Class cls, boolean requireExact) {
        Class[] arguments = new Class[args.length];

        for(int i = 0; i != args.length; ++i) {
            if(args[i] != null) {
                arguments[i] = args[i].getClass();
            }
        }

        return getBestConstructorCandidate(arguments, cls, requireExact);
    }

    public static Constructor getBestConstructorCandidate(Class[] arguments, Class cls, boolean requireExact) {
        Constructor bestCandidate = null;
        int bestScore = 0;
        int score = 0;
        Integer hash = Integer.valueOf(createClassSignatureHash(cls, arguments));
        Object cache = (Map)RESOLVED_CONST_CACHE.get(cls);
        WeakReference ref;
        if(cache != null && (ref = (WeakReference)((Map)cache).get(hash)) != null && (bestCandidate = (Constructor)ref.get()) != null) {
            return bestCandidate;
        } else {
            Constructor[] arr$ = getConstructors(cls);
            int len$ = arr$.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                Constructor construct = arr$[i$];
                Class[] parmTypes;
                if((parmTypes = getConstructors(construct)).length == arguments.length || construct.isVarArgs()) {
                    if(arguments.length == 0 && parmTypes.length == 0) {
                        return construct;
                    }

                    for(int i = 0; i != arguments.length; ++i) {
                        if(arguments[i] == null) {
                            if(parmTypes[i].isPrimitive()) {
                                score = 0;
                                break;
                            }

                            score += 5;
                        } else if(parmTypes[i] == arguments[i]) {
                            score += 6;
                        } else if(parmTypes[i].isPrimitive() && boxPrimitive(parmTypes[i]) == arguments[i]) {
                            score += 5;
                        } else if(arguments[i].isPrimitive() && unboxPrimitive(arguments[i]) == parmTypes[i]) {
                            score += 5;
                        } else if(isNumericallyCoercible(arguments[i], parmTypes[i])) {
                            score += 4;
                        } else if(boxPrimitive(parmTypes[i]).isAssignableFrom(boxPrimitive(arguments[i])) && parmTypes[i] != Object.class) {
                            score += 3 + scoreInterface(parmTypes[i], arguments[i]);
                        } else if(!requireExact && DataConversion.canConvert(parmTypes[i], arguments[i])) {
                            if(parmTypes[i].isArray() && arguments[i].isArray()) {
                                ++score;
                            } else if(parmTypes[i] == Character.TYPE && arguments[i] == String.class) {
                                ++score;
                            }

                            ++score;
                        } else {
                            if(parmTypes[i] != Object.class && arguments[i] != NullType.class) {
                                score = 0;
                                break;
                            }

                            ++score;
                        }
                    }

                    if(score != 0 && score > bestScore) {
                        bestCandidate = construct;
                        bestScore = score;
                    }

                    score = 0;
                }
            }

            if(bestCandidate != null) {
                if(cache == null) {
                    RESOLVED_CONST_CACHE.put(cls, (Map<Integer, WeakReference<Constructor>>) (cache = new WeakHashMap()));
                }

                ((Map)cache).put(hash, new WeakReference(bestCandidate));
            }

            return bestCandidate;
        }
    }

    public static Class createClass(String className, ParserContext pCtx) throws ClassNotFoundException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Object cache = (Map)CLASS_RESOLVER_CACHE.get(classLoader);
        if(cache == null) {
            CLASS_RESOLVER_CACHE.put(classLoader, (Map<String, WeakReference<Class>>) (cache = new WeakHashMap(10)));
        }

        WeakReference ref;
        Class cls;
        if((ref = (WeakReference)((Map)cache).get(className)) != null && (cls = (Class)ref.get()) != null) {
            return cls;
        } else {
            try {
                cls = pCtx == null?Class.forName(className, true, Thread.currentThread().getContextClassLoader()):Class.forName(className, true, pCtx.getParserConfiguration().getClassLoader());
            } catch (ClassNotFoundException var7) {
                cls = Class.forName(className, true, Thread.currentThread().getContextClassLoader());
            }

            ((Map)cache).put(className, new WeakReference(cls));
            return cls;
        }
    }

    public static Constructor[] getConstructors(Class cls) {
        WeakReference ref = (WeakReference)CLASS_CONSTRUCTOR_CACHE.get(cls);
        Constructor[] cns;
        if(ref != null && (cns = (Constructor[])ref.get()) != null) {
            return cns;
        } else {
            CLASS_CONSTRUCTOR_CACHE.put(cls, new WeakReference(cns = cls.getConstructors()));
            return cns;
        }
    }

    public static String[] captureContructorAndResidual(char[] cs, int start, int offset) {
        int depth = 0;
        int end = start + offset;

        for(int i = start; i < end; ++i) {
            switch(cs[i]) {
                case '(':
                    ++depth;
                    break;
                case ')':
                    if(1 == depth--) {
                        String[] var10000 = new String[2];
                        ++i;
                        var10000[0] = createStringTrimmed(cs, start, i - start);
                        var10000[1] = createStringTrimmed(cs, i, end - i);
                        return var10000;
                    }
            }
        }

        return new String[]{new String(cs, start, offset)};
    }

    public static Class boxPrimitive(Class cls) {
        return cls != Integer.TYPE && cls != Integer.class?(cls != int[].class && cls != Integer[].class?(cls != Character.TYPE && cls != Character.class?(cls != char[].class && cls != Character[].class?(cls != Long.TYPE && cls != Long.class?(cls != long[].class && cls != Long[].class?(cls != Short.TYPE && cls != Short.class?(cls != short[].class && cls != Short[].class?(cls != Double.TYPE && cls != Double.class?(cls != double[].class && cls != Double[].class?(cls != Float.TYPE && cls != Float.class?(cls != float[].class && cls != Float[].class?(cls != Boolean.TYPE && cls != Boolean.class?(cls != boolean[].class && cls != Boolean[].class?(cls != Byte.TYPE && cls != Byte.class?(cls != byte[].class && cls != Byte[].class?cls:Byte[].class):Byte.class):Boolean[].class):Boolean.class):Float[].class):Float.class):Double[].class):Double.class):Short[].class):Short.class):Long[].class):Long.class):Character[].class):Character.class):Integer[].class):Integer.class;
    }

    public static Class unboxPrimitive(Class cls) {
        return cls != Integer.class && cls != Integer.TYPE?(cls != Integer[].class && cls != int[].class?(cls != Long.class && cls != Long.TYPE?(cls != Long[].class && cls != long[].class?(cls != Character.class && cls != Character.TYPE?(cls != Character[].class && cls != char[].class?(cls != Short.class && cls != Short.TYPE?(cls != Short[].class && cls != short[].class?(cls != Double.class && cls != Double.TYPE?(cls != Double[].class && cls != double[].class?(cls != Float.class && cls != Float.TYPE?(cls != Float[].class && cls != float[].class?(cls != Boolean.class && cls != Boolean.TYPE?(cls != Boolean[].class && cls != boolean[].class?(cls != Byte.class && cls != Byte.TYPE?(cls != Byte[].class && cls != byte[].class?cls:byte[].class):Byte.TYPE):boolean[].class):Boolean.TYPE):float[].class):Float.TYPE):double[].class):Double.TYPE):short[].class):Short.TYPE):char[].class):Character.TYPE):long[].class):Long.TYPE):int[].class):Integer.TYPE;
    }

    public static boolean containsCheck(Object compareTo, Object compareTest) {
        if(compareTo == null) {
            return false;
        } else if(compareTo instanceof String) {
            return ((String)compareTo).contains(String.valueOf(compareTest));
        } else if(compareTo instanceof Collection) {
            return ((Collection)compareTo).contains(compareTest);
        } else if(compareTo instanceof Map) {
            return ((Map)compareTo).containsKey(compareTest);
        } else {
            if(compareTo.getClass().isArray()) {
                Object[] arr$ = (Object[])((Object[])compareTo);
                int len$ = arr$.length;

                for(int i$ = 0; i$ < len$; ++i$) {
                    Object o = arr$[i$];
                    if(compareTest == null && o == null) {
                        return true;
                    }

                    if(((Boolean)MathProcessor.doOperations(o, 18, compareTest)).booleanValue()) {
                        return true;
                    }
                }
            }

            return false;
        }
    }

    public static int createClassSignatureHash(Class declaring, Class[] sig) {
        int hash = 0;
        Class[] arr$ = sig;
        int len$ = sig.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            Class cls = arr$[i$];
            if(cls != null) {
                hash += cls.hashCode();
            }
        }

        return hash + sig.length + declaring.hashCode();
    }

    public static int handleEscapeSequence(char[] escapeStr, int pos) {
        escapeStr[pos - 1] = 0;
        int s;
        switch(escapeStr[pos]) {
            case '\"':
                escapeStr[pos] = 34;
                return 1;
            case '\'':
                escapeStr[pos] = 39;
                return 1;
            case '\\':
                escapeStr[pos] = 92;
                return 1;
            case 'b':
                escapeStr[pos] = 8;
                return 1;
            case 'f':
                escapeStr[pos] = 12;
                return 1;
            case 'n':
                escapeStr[pos] = 10;
                return 1;
            case 'r':
                escapeStr[pos] = 13;
                return 1;
            case 't':
                escapeStr[pos] = 9;
                return 1;
            case 'u':
                s = pos;
                if(pos + 4 > escapeStr.length) {
                    throw new CompileException("illegal unicode escape sequence", escapeStr, pos);
                } else {
                    do {
                        do {
                            ++pos;
                            if(pos - s == 5) {
                                escapeStr[s - 1] = (char)Integer.decode("0x" + new String(escapeStr, s + 1, 4)).intValue();
                                escapeStr[s] = 0;
                                escapeStr[s + 1] = 0;
                                escapeStr[s + 2] = 0;
                                escapeStr[s + 3] = 0;
                                escapeStr[s + 4] = 0;
                                return 5;
                            }
                        } while(escapeStr[pos] > 47 && escapeStr[pos] < 58);
                    } while(escapeStr[pos] > 64 && escapeStr[pos] < 71);

                    throw new CompileException("illegal unicode escape sequence", escapeStr, pos);
                }
            default:
                s = pos;

                while(true) {
                    if(escapeStr[pos] >= 48 && escapeStr[pos] < 56) {
                        if(pos != s && escapeStr[s] > 51) {
                            escapeStr[s - 1] = (char)Integer.decode("0" + new String(escapeStr, s, pos - s + 1)).intValue();
                            escapeStr[s] = 0;
                            escapeStr[s + 1] = 0;
                            return 2;
                        }

                        if(pos - s == 2) {
                            escapeStr[s - 1] = (char)Integer.decode("0" + new String(escapeStr, s, pos - s + 1)).intValue();
                            escapeStr[s] = 0;
                            escapeStr[s + 1] = 0;
                            escapeStr[s + 2] = 0;
                            return 3;
                        }

                        if(pos + 1 != escapeStr.length && escapeStr[pos] >= 48 && escapeStr[pos] <= 55) {
                            ++pos;
                            continue;
                        }

                        escapeStr[s - 1] = (char)Integer.decode("0" + new String(escapeStr, s, pos - s + 1)).intValue();
                        escapeStr[s] = 0;
                        return 1;
                    }

                    throw new CompileException("illegal escape sequence: " + escapeStr[pos], escapeStr, pos);
                }
        }
    }

    public static char[] createShortFormOperativeAssignment(String name, char[] statement, int start, int offset, int operation) {
        if(operation == -1) {
            return statement;
        } else {
            short op = 0;
            switch(operation) {
                case 0:
                    op = 43;
                    break;
                case 1:
                    op = 45;
                    break;
                case 2:
                    op = 42;
                    break;
                case 3:
                    op = 47;
                    break;
                case 4:
                    op = 37;
                case 5:
                case 8:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                default:
                    break;
                case 6:
                    op = 38;
                    break;
                case 7:
                    op = 124;
                    break;
                case 9:
                    op = 187;
                    break;
                case 10:
                    op = 171;
                    break;
                case 11:
                    op = 172;
                    break;
                case 20:
                    op = 35;
            }

            char[] stmt;
            System.arraycopy(name.toCharArray(), 0, stmt = new char[name.length() + offset + 1], 0, name.length());
            stmt[name.length()] = (char)op;
            System.arraycopy(statement, start, stmt, name.length() + 1, offset);
            return stmt;
        }
    }

    public static ClassImportResolverFactory findClassImportResolverFactory(VariableResolverFactory factory) {
        for(VariableResolverFactory v = factory; v != null; v = v.getNextFactory()) {
            if(v instanceof ClassImportResolverFactory) {
                return (ClassImportResolverFactory)v;
            }
        }

        if(factory == null) {
            throw new OptimizationFailure("unable to import classes.  no variable resolver factory available.");
        } else {
            return (ClassImportResolverFactory)ResolverTools.appendFactory(factory, new ClassImportResolverFactory());
        }
    }

    public static Class findClass(VariableResolverFactory factory, String name, ParserContext ctx) throws ClassNotFoundException {
        try {
            return AbstractParser.LITERALS.containsKey(name)?(Class)AbstractParser.LITERALS.get(name):(factory != null && factory.isResolveable(name)?(Class)factory.getVariableResolver(name).getValue():(ctx != null && ctx.hasImport(name)?ctx.getImport(name):createClass(name, ctx)));
        } catch (ClassNotFoundException var4) {
            throw var4;
        } catch (Exception var5) {
            throw new RuntimeException("class not found: " + name, var5);
        }
    }

    public static char[] subsetTrimmed(char[] array, int start, int length) {
        if(length <= 0) {
            return new char[0];
        } else {
            int end;
            for(end = start + length; end > 0 && isWhitespace(array[end - 1]); --end) {
                ;
            }

            while(isWhitespace(array[start]) && start < end) {
                ++start;
            }

            length = end - start;
            return length == 0?new char[0]:subset(array, start, length);
        }
    }

    public static char[] subset(char[] array, int start, int length) {
        char[] newArray = new char[length];

        for(int i = 0; i < newArray.length; ++i) {
            newArray[i] = array[i + start];
        }

        return newArray;
    }

    public static char[] subset(char[] array, int start) {
        char[] newArray = new char[array.length - start];

        for(int i = 0; i < newArray.length; ++i) {
            newArray[i] = array[i + start];
        }

        return newArray;
    }

    public static int resolveType(Object o) {
        return o == null?0:__resolveType(o.getClass());
    }

    public static int __resolveType(Class cls) {
        Integer code = (Integer)typeCodes.get(cls);
        return code == null?(cls != null && Collection.class.isAssignableFrom(cls)?50:0):code.intValue();
    }

    public static boolean isNumericallyCoercible(Class target, Class parm) {
        Class boxedTarget = target.isPrimitive()?boxPrimitive(target):target;
        if(boxedTarget != null && Number.class.isAssignableFrom(target)) {
            Class var10000 = parm.isPrimitive()?boxPrimitive(parm):parm;
            boxedTarget = var10000;
            if(var10000 != null) {
                return Number.class.isAssignableFrom(boxedTarget);
            }
        }

        return false;
    }

    public static Object narrowType(BigDecimal result, int returnTarget) {
        return returnTarget != 109 && result.scale() <= 0?(returnTarget != 107 && result.longValue() <= 2147483647L?Integer.valueOf(result.intValue()):Long.valueOf(result.longValue())):Double.valueOf(result.doubleValue());
    }

    public static Method determineActualTargetMethod(Method method) {
        String name = method.getName();
        Class[] arr$ = method.getDeclaringClass().getInterfaces();
        int len$ = arr$.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            Class cls = arr$[i$];
            Method[] arr$1 = cls.getMethods();
            int len$1 = arr$1.length;

            for(int i$1 = 0; i$1 < len$1; ++i$1) {
                Method meth = arr$1[i$1];
                if(meth.getParameterTypes().length == 0 && name.equals(meth.getName())) {
                    return meth;
                }
            }
        }

        return null;
    }

    public static int captureToNextTokenJunction(char[] expr, int cursor, int end, ParserContext pCtx) {
        while(cursor != expr.length) {
            switch(expr[cursor]) {
                case '(':
                case '{':
                    return cursor;
                case '[':
                    cursor = balancedCaptureWithLineAccounting(expr, cursor, end, '[', pCtx) + 1;
                    break;
                default:
                    if(isWhitespace(expr[cursor])) {
                        return cursor;
                    }

                    ++cursor;
            }
        }

        return cursor;
    }

    public static int nextNonBlank(char[] expr, int cursor) {
        if(cursor + 1 >= expr.length) {
            throw new CompileException("unexpected end of statement", expr, cursor);
        } else {
            int i;
            for(i = cursor; i != expr.length && isWhitespace(expr[i]); ++i) {
                ;
            }

            return i;
        }
    }

    public static int skipWhitespace(char[] expr, int cursor) {
        while(true) {
            while(cursor != expr.length) {
                switch(expr[cursor]) {
                    case '\n':
                    case '\r':
                        ++cursor;
                        break;
                    case '/':
                        if(cursor + 1 != expr.length) {
                            switch(expr[cursor + 1]) {
                                case '*':
                                    int len = expr.length - 1;

                                    for(expr[cursor++] = 32; cursor != len && (expr[cursor] != 42 || expr[cursor + 1] != 47); expr[cursor++] = 32) {
                                        ;
                                    }

                                    if(cursor != len) {
                                        expr[cursor++] = expr[cursor++] = 32;
                                    }
                                    continue;
                                case '/':
                                    for(expr[cursor++] = 32; cursor != expr.length && expr[cursor] != 10; expr[cursor++] = 32) {
                                        ;
                                    }

                                    if(cursor != expr.length) {
                                        expr[cursor++] = 32;
                                    }
                                    continue;
                                default:
                                    return cursor;
                            }
                        }
                    default:
                        if(!isWhitespace(expr[cursor])) {
                            return cursor;
                        }

                        ++cursor;
                }
            }

            return cursor;
        }
    }

    public static boolean isStatementNotManuallyTerminated(char[] expr, int cursor) {
        if(cursor >= expr.length) {
            return false;
        } else {
            int c;
            for(c = cursor; c != expr.length && isWhitespace(expr[c]); ++c) {
                ;
            }

            return c == expr.length || expr[c] != 59;
        }
    }

    public static int captureToEOS(char[] expr, int cursor, int end, ParserContext pCtx) {
        for(; cursor != expr.length; ++cursor) {
            switch(expr[cursor]) {
                case '\"':
                case '\'':
                    cursor = captureStringLiteral(expr[cursor], expr, cursor, expr.length);
                    break;
                case '(':
                case '[':
                case '{':
                    if((cursor = balancedCaptureWithLineAccounting(expr, cursor, end, expr[cursor], pCtx)) >= expr.length) {
                        return cursor;
                    }
                    break;
                case ',':
                case ';':
                case '}':
                    return cursor;
            }
        }

        return cursor;
    }

    public static int trimLeft(char[] expr, int start, int pos) {
        if(pos > expr.length) {
            pos = expr.length;
        }

        while(pos != 0 && pos >= start && isWhitespace(expr[pos - 1])) {
            --pos;
        }

        return pos;
    }

    public static int trimRight(char[] expr, int pos) {
        while(pos != expr.length && isWhitespace(expr[pos])) {
            ++pos;
        }

        return pos;
    }

    public static char[] subArray(char[] expr, int start, int end) {
        if(start >= end) {
            return new char[0];
        } else {
            char[] newA = new char[end - start];

            for(int i = 0; i != newA.length; ++i) {
                newA[i] = expr[i + start];
            }

            return newA;
        }
    }

    public static int balancedCapture(char[] chars, int start, char type) {
        return balancedCapture(chars, start, chars.length, type);
    }

    public static int balancedCapture(char[] chars, int start, int end, char type) {
        int depth = 1;
        char term = type;
        switch(type) {
            case '(':
                term = 41;
                break;
            case '[':
                term = 93;
                break;
            case '{':
                term = 125;
        }

        if(type == term) {
            ++start;

            while(start < end) {
                if(chars[start] == type) {
                    return start;
                }

                ++start;
            }
        } else {
            ++start;

            for(; start < end; ++start) {
                if(start < end && chars[start] == 47) {
                    if(start + 1 == end) {
                        return start;
                    }

                    if(chars[start + 1] == 47) {
                        ++start;

                        while(start < end && chars[start] != 10) {
                            ++start;
                        }
                    } else if(chars[start + 1] == 42) {
                        start += 2;

                        label74:
                        while(start < end) {
                            switch(chars[start]) {
                                case '*':
                                    if(start + 1 < end && chars[start + 1] == 47) {
                                        break label74;
                                    }
                                case '\n':
                                case '\r':
                                default:
                                    ++start;
                            }
                        }
                    }
                }

                if(start == end) {
                    return start;
                }

                if(chars[start] != 39 && chars[start] != 34) {
                    if(chars[start] == type) {
                        ++depth;
                    } else if(chars[start] == term) {
                        --depth;
                        if(depth == 0) {
                            return start;
                        }
                    }
                } else {
                    start = captureStringLiteral(chars[start], chars, start, end);
                }
            }
        }

        switch(type) {
            case '(':
                throw new CompileException("unbalanced braces ( ... )", chars, start);
            case '[':
                throw new CompileException("unbalanced braces [ ... ]", chars, start);
            case '{':
                throw new CompileException("unbalanced braces { ... }", chars, start);
            default:
                throw new CompileException("unterminated string literal", chars, start);
        }
    }

    public static int balancedCaptureWithLineAccounting(char[] chars, int start, int end, char type, ParserContext pCtx) {
        int depth = 1;
        int st = start;
        char term = type;
        switch(type) {
            case '(':
                term = 41;
                break;
            case '[':
                term = 93;
                break;
            case '{':
                term = 125;
        }

        if(type == term) {
            ++start;

            while(start != end) {
                if(chars[start] == type) {
                    return start;
                }

                ++start;
            }
        } else {
            int lines = 0;
            ++start;

            for(; start < end; ++start) {
                if(isWhitespace(chars[start])) {
                    switch(chars[start]) {
                        case '\n':
                            if(pCtx != null) {
                                pCtx.setLineOffset((short)start);
                            }

                            ++lines;
                            break;
                        case '\r':
                            continue;
                    }
                } else if(start < end && chars[start] == 47) {
                    if(start + 1 == end) {
                        return start;
                    }

                    if(chars[start + 1] == 47) {
                        ++start;

                        while(start < end && chars[start] != 10) {
                            ++start;
                        }
                    } else if(chars[start + 1] == 42) {
                        start += 2;

                        label96:
                        while(start != end) {
                            switch(chars[start]) {
                                case '*':
                                    if(start + 1 < end && chars[start + 1] == 47) {
                                        break label96;
                                    }
                                case '\n':
                                case '\r':
                                    if(pCtx != null) {
                                        pCtx.setLineOffset((short)start);
                                    }

                                    ++lines;
                                default:
                                    ++start;
                            }
                        }
                    }
                }

                if(start == end) {
                    return start;
                }

                if(chars[start] != 39 && chars[start] != 34) {
                    if(chars[start] == type) {
                        ++depth;
                    } else if(chars[start] == term) {
                        --depth;
                        if(depth == 0) {
                            if(pCtx != null) {
                                pCtx.incrementLineCount(lines);
                            }

                            return start;
                        }
                    }
                } else {
                    start = captureStringLiteral(chars[start], chars, start, end);
                }
            }
        }

        switch(type) {
            case '(':
                throw new CompileException("unbalanced braces ( ... )", chars, st);
            case '[':
                throw new CompileException("unbalanced braces [ ... ]", chars, st);
            case '{':
                throw new CompileException("unbalanced braces { ... }", chars, st);
            default:
                throw new CompileException("unterminated string literal", chars, st);
        }
    }

    public static String handleStringEscapes(char[] input) {
        int escapes = 0;

        for(int processedEscapeString = 0; processedEscapeString < input.length; ++processedEscapeString) {
            if(input[processedEscapeString] == 92) {
                ++processedEscapeString;
                escapes += handleEscapeSequence(input, processedEscapeString);
            }
        }

        if(escapes == 0) {
            return new String(input);
        } else {
            char[] var8 = new char[input.length - escapes];
            int cursor = 0;
            char[] arr$ = input;
            int len$ = input.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                char aName = arr$[i$];
                if(aName != 0) {
                    var8[cursor++] = aName;
                }
            }

            return new String(var8);
        }
    }

    public static int captureStringLiteral(char type, char[] expr, int cursor, int end) {
        while(true) {
            ++cursor;
            if(cursor >= end || expr[cursor] == type) {
                if(cursor < end && expr[cursor] == type) {
                    return cursor;
                }

                throw new CompileException("unterminated string literal", expr, cursor);
            }

            if(expr[cursor] == 92) {
                ++cursor;
            }
        }
    }

    public static void parseWithExpressions(String nestParm, char[] block, int start, int offset, Object ctx, VariableResolverFactory factory) {
        int _st = start;
        int _end = -1;
        int end = start + offset;
        int oper = -1;
        String parm = "";

        for(int e = start; e < end; ++e) {
            switch(block[e]) {
                case '\"':
                case '\'':
                case '(':
                case '[':
                case '{':
                    e = balancedCapture(block, e, end, block[e]);
                    break;
                case '%':
                case '*':
                case '+':
                case '-':
                    if(e + 1 < end && block[e + 1] == 61) {
                        oper = opLookup(block[e]);
                    }
                    break;
                case ',':
                    if(_end == -1) {
                        _end = e;
                    }

                    if(parm == null) {
                        try {
                            if(nestParm == null) {
                                MVEL.eval(new String(block, _st, _end - _st), ctx, factory);
                            } else {
                                MVEL.eval((new StringBuilder(nestParm)).append('.').append(block, _st, _end - _st).toString(), ctx, factory);
                            }
                        } catch (CompileException var14) {
                            var14.setCursor(_st + (var14.getCursor() - (var14.getExpr().length - offset)));
                            var14.setExpr(block);
                            throw var14;
                        }

                        oper = -1;
                        ++e;
                        _st = e;
                    } else {
                        try {
                            if(oper != -1) {
                                if(nestParm == null) {
                                    throw new CompileException("operative assignment not possible here", block, start);
                                }

                                String var16 = new String(createShortFormOperativeAssignment(nestParm + "." + parm, block, _st, _end - _st, oper));
                                MVEL.setProperty(ctx, parm, MVEL.eval(var16, ctx, factory));
                            } else {
                                MVEL.setProperty(ctx, parm, MVEL.eval(block, _st, _end - _st, ctx, factory));
                            }
                        } catch (CompileException var13) {
                            var13.setCursor(_st + (var13.getCursor() - (var13.getExpr().length - offset)));
                            var13.setExpr(block);
                            throw var13;
                        }

                        parm = null;
                        oper = -1;
                        ++e;
                        _st = e;
                    }

                    _end = -1;
                    break;
                case '/':
                    if(e < end && block[e + 1] == 47) {
                        while(e < end && block[e] != 10) {
                            block[e++] = 32;
                        }

                        if(parm == null) {
                            _st = e;
                        }
                    } else if(e < end && block[e + 1] == 42) {
                        for(int e1 = end - 1; e < e1 && (block[e] != 42 || block[e + 1] != 47); block[e++] = 32) {
                            ;
                        }

                        block[e++] = 32;
                        block[e++] = 32;
                        if(parm == null) {
                            _st = e;
                        }
                    } else if(e < end && block[e + 1] == 61) {
                        oper = 3;
                    }
                    break;
                case '=':
                    parm = (new String(block, _st, e - _st - (oper != -1?1:0))).trim();
                    _st = e + 1;
            }
        }

        _end = end;
        if(_st != end) {
            try {
                if(parm != null && !"".equals(parm)) {
                    if(oper != -1) {
                        if(nestParm == null) {
                            throw new CompileException("operative assignment not possible here", block, start);
                        }

                        String var17 = new String(createShortFormOperativeAssignment(nestParm + "." + parm, block, _st, _end - _st, oper));
                        MVEL.setProperty(ctx, parm, MVEL.eval(var17, ctx, factory));
                    } else {
                        MVEL.setProperty(ctx, parm, MVEL.eval(block, _st, end - _st, ctx, factory));
                    }
                } else if(nestParm == null) {
                    MVEL.eval(new String(block, _st, _end - _st), ctx, factory);
                } else {
                    MVEL.eval((new StringAppender(nestParm)).append('.').append(block, _st, _end - _st).toString(), ctx, factory);
                }
            } catch (CompileException var15) {
                var15.setCursor(_st + (var15.getCursor() - (var15.getExpr().length - offset)));
                var15.setExpr(block);
                throw var15;
            }
        }

    }

    public static Object handleNumericConversion(char[] val, int start, int offset) {
        if(offset != 1 && val[start] == 48 && val[start + 1] != 46) {
            if(!isDigit(val[offset - 1])) {
                switch(val[offset - 1]) {
                    case 'D':
                        return BigDecimal.valueOf(Long.decode(new String(val, start, offset - 1)).longValue());
                    case 'I':
                        return BigInteger.valueOf(Long.decode(new String(val, start, offset - 1)).longValue());
                    case 'L':
                    case 'l':
                        return Long.decode(new String(val, start, offset - 1));
                }
            }

            return Integer.decode(new String(val));
        } else if(!isDigit(val[start + offset - 1])) {
            switch(val[start + offset - 1]) {
                case '.':
                case 'D':
                case 'd':
                    return Double.valueOf(Double.parseDouble(new String(val, start, offset - 1)));
                case 'B':
                    return new BigDecimal(new String(val, start, offset - 1));
                case 'F':
                case 'f':
                    return Float.valueOf(Float.parseFloat(new String(val, start, offset - 1)));
                case 'I':
                    return new BigInteger(new String(val, start, offset - 1));
                case 'L':
                case 'l':
                    return Long.valueOf(Long.parseLong(new String(val, start, offset - 1)));
                default:
                    throw new CompileException("unrecognized numeric literal", val, start);
            }
        } else {
            switch(numericTest(val, start, offset)) {
                case 101:
                    return Integer.valueOf(Integer.parseInt(new String(val, start, offset)));
                case 102:
                    return Long.valueOf(Long.parseLong(new String(val, start, offset)));
                case 103:
                    return Double.valueOf(Double.parseDouble(new String(val, start, offset)));
                case 104:
                    return Float.valueOf(Float.parseFloat(new String(val, start, offset)));
                case 105:
                case 106:
                case 107:
                case 108:
                case 109:
                default:
                    return new String(val, start, offset);
                case 110:
                    return new BigDecimal(val, MathContext.DECIMAL128);
            }
        }
    }

    public static boolean isNumeric(Object val) {
        if(val == null) {
            return false;
        } else {
            Class clz;
            if(val instanceof Class) {
                clz = (Class)val;
            } else {
                clz = val.getClass();
            }

            return clz == Integer.TYPE || clz == Long.TYPE || clz == Short.TYPE || clz == Double.TYPE || clz == Float.TYPE || Number.class.isAssignableFrom(clz);
        }
    }

    public static int numericTest(char[] val, int start, int offset) {
        boolean fp = false;
        int i = start;
        if(offset > 1) {
            if(val[start] == 45) {
                i = start + 1;
            } else if(val[start] == 126) {
                i = start + 1;
                if(val[start + 1] == 45) {
                    ++i;
                }
            }
        }

        for(int end = start + offset; i < end; ++i) {
            char c;
            if(!isDigit(c = val[i])) {
                switch(c) {
                    case '.':
                        fp = true;
                        break;
                    case 'E':
                    case 'e':
                        fp = true;
                        if(i++ < end && val[i] == 45) {
                            ++i;
                        }
                        break;
                    default:
                        return -1;
                }
            }
        }

        if(offset != 0) {
            if(fp) {
                return 103;
            } else if(offset > 9) {
                return 102;
            } else {
                return 101;
            }
        } else {
            return -1;
        }
    }

    public static boolean isNumber(Object val) {
        return val == null?false:(val instanceof String?isNumber((String)val):(val instanceof char[]?isNumber(new String((char[])((char[])val))):val instanceof Integer || val instanceof BigDecimal || val instanceof BigInteger || val instanceof Float || val instanceof Double || val instanceof Long || val instanceof Short || val instanceof Character));
    }

    public static boolean isNumber(String val) {
        int len = val.length();
        boolean f = true;
        int i = 0;
        if(len > 1) {
            if(val.charAt(0) == 45) {
                ++i;
            } else if(val.charAt(0) == 126) {
                ++i;
                if(val.charAt(1) == 45) {
                    ++i;
                }
            }
        }

        for(; i < len; ++i) {
            char c;
            if(!isDigit(c = val.charAt(i))) {
                if(c != 46 || !f) {
                    return false;
                }

                f = false;
            }
        }

        return len > 0;
    }

    public static boolean isNumber(char[] val, int start, int offset) {
        boolean f = true;
        int i = start;
        int end = start + offset;
        if(offset > 1) {
            switch(val[start]) {
                case '-':
                    if(val[start + 1] == 45) {
                        i = start + 1;
                    }
                case '~':
                    ++i;
            }
        }

        for(; i < end; ++i) {
            char c;
            if(!isDigit(c = val[i])) {
                if(f && c == 46) {
                    f = false;
                } else {
                    if(offset != 1 && i == start + offset - 1) {
                        switch(c) {
                            case '.':
                                throw new CompileException("invalid number literal: " + new String(val), val, start);
                            case 'B':
                            case 'D':
                            case 'F':
                            case 'I':
                            case 'L':
                            case 'd':
                            case 'f':
                            case 'l':
                                return true;
                            default:
                                return false;
                        }
                    }

                    if(i == start + 1 && c == 120 && val[start] == 48) {
                        ++i;

                        while(i < end) {
                            if(!isDigit(c = val[i]) && (c < 65 || c > 70) && (c < 97 || c > 102)) {
                                if(i == offset - 1) {
                                    switch(c) {
                                        case 'B':
                                        case 'I':
                                        case 'L':
                                        case 'l':
                                            return true;
                                    }
                                }

                                return false;
                            }

                            ++i;
                        }

                        return offset - 2 > 0;
                    }

                    if(i == start || i + 1 >= offset || c != 69 && c != 101) {
                        if(i != start) {
                            throw new CompileException("invalid number literal: " + new String(val, start, offset), val, start);
                        } else {
                            return false;
                        }
                    }

                    ++i;
                    if(val[i] == 45 || val[i] == 43) {
                        ++i;
                    }
                }
            }
        }

        return end > start;
    }

    public static int find(char[] c, int start, int offset, char find) {
        int length = start + offset;

        for(int i = start; i < length; ++i) {
            if(c[i] == find) {
                return i;
            }
        }

        return -1;
    }

    public static int findLast(char[] c, int start, int offset, char find) {
        for(int i = start + offset; i >= start; --i) {
            if(c[i] == find) {
                return i;
            }
        }

        return -1;
    }

    public static String createStringTrimmed(char[] s) {
        int start = 0;

        int end;
        for(end = s.length; start != end && s[start] < 33; ++start) {
            ;
        }

        while(end != start && s[end - 1] < 33) {
            --end;
        }

        return new String(s, start, end - start);
    }

    public static String createStringTrimmed(char[] s, int start, int length) {
        if((length += start) > s.length) {
            return new String(s);
        } else {
            while(start != length && s[start] < 33) {
                ++start;
            }

            while(length != start && s[length - 1] < 33) {
                --length;
            }

            return new String(s, start, length - start);
        }
    }

    public static boolean endsWith(char[] c, int start, int offset, char[] test) {
        if(test.length > c.length) {
            return false;
        } else {
            int tD = test.length - 1;
            int cD = start + offset - 1;

            do {
                if(tD < 0) {
                    return true;
                }
            } while(c[cD--] == test[tD--]);

            return false;
        }
    }

    public static boolean isIdentifierPart(int c) {
        return c > 96 && c < 123 || c > 64 && c < 91 || c > 47 && c < 58 || c == 95 || c == 36 || Character.isJavaIdentifierPart(c);
    }

    public static boolean isDigit(int c) {
        return c > 47 && c < 58;
    }

    public static float similarity(String s1, String s2) {
        if(s1 != null && s2 != null) {
            char[] c1 = s1.toCharArray();
            char[] c2 = s2.toCharArray();
            float same = 0.0F;
            int cur1 = 0;
            char[] comp;
            char[] against;
            float baselength;
            if(c1.length > c2.length) {
                baselength = (float)c1.length;
                comp = c1;
                against = c2;
            } else {
                baselength = (float)c2.length;
                comp = c2;
                against = c1;
            }

            for(; cur1 < comp.length && cur1 < against.length; ++cur1) {
                if(comp[cur1] == against[cur1]) {
                    ++same;
                }
            }

            return same / baselength;
        } else {
            return s1 == null && s2 == null?1.0F:0.0F;
        }
    }

    public static int findAbsoluteLast(char[] array) {
        int depth = 0;

        for(int i = array.length - 1; i >= 0; --i) {
            if(array[i] == 93) {
                ++depth;
            }

            if(array[i] == 91) {
                --depth;
            }

            if(depth == 0 && array[i] == 46 || array[i] == 91) {
                return i;
            }
        }

        return -1;
    }

    public static Class getBaseComponentType(Class cls) {
        while(cls.isArray()) {
            cls = cls.getComponentType();
        }

        return cls;
    }

    public static Class getSubComponentType(Class cls) {
        if(cls.isArray()) {
            cls = cls.getComponentType();
        }

        return cls;
    }

    public static boolean isJunct(char c) {
        switch(c) {
            case '(':
            case '[':
                return true;
            default:
                return isWhitespace(c);
        }
    }

    public static int opLookup(char c) {
        switch(c) {
            case '%':
                return 4;
            case '&':
                return 6;
            case '*':
                return 2;
            case '+':
                return 0;
            case '/':
                return 3;
            case '^':
                return 8;
            case '|':
                return 7;
            case '«':
                return 10;
            case '¬':
                return 11;
            case '»':
                return 9;
            default:
                return -1;
        }
    }

    public static boolean isReservedWord(String name) {
        return AbstractParser.LITERALS.containsKey(name) || AbstractParser.OPERATORS.containsKey(name);
    }

    public static boolean isNotValidNameorLabel(String name) {
        char[] arr$ = name.toCharArray();
        int len$ = arr$.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            char c = arr$[i$];
            if(c == 46) {
                return true;
            }

            if(!isIdentifierPart(c)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isPropertyOnly(char[] array, int start, int end) {
        end = start + (end - start);

        for(int i = start; i < end; ++i) {
            if(!isIdentifierPart(array[i])) {
                return false;
            }
        }

        return true;
    }

    public static void checkNameSafety(String name) {
        if(isReservedWord(name)) {
            throw new RuntimeException("illegal use of reserved word: " + name);
        } else if(isDigit(name.charAt(0))) {
            throw new RuntimeException("not an identifier: " + name);
        }
    }

    public static FileWriter getDebugFileWriter() throws IOException {
        return new FileWriter(new File(MVEL.getDebuggingOutputFileName()), true);
    }

    public static boolean isPrimitiveWrapper(Class clazz) {
        return clazz == Integer.class || clazz == Boolean.class || clazz == Long.class || clazz == Double.class || clazz == Float.class || clazz == Character.class || clazz == Short.class || clazz == Byte.class;
    }

    public static Serializable subCompileExpression(char[] expression) {
        return _optimizeTree((new ExpressionCompiler(expression))._compile());
    }

    public static Serializable subCompileExpression(char[] expression, ParserContext ctx) {
        ExpressionCompiler c = new ExpressionCompiler(expression);
        if(ctx != null) {
            c.setPCtx(ctx);
        }

        return _optimizeTree(c._compile());
    }

    public static Serializable subCompileExpression(char[] expression, int start, int offset) {
        return _optimizeTree((new ExpressionCompiler(expression, start, offset))._compile());
    }

    public static Serializable subCompileExpression(char[] expression, int start, int offset, ParserContext ctx) {
        ExpressionCompiler c = new ExpressionCompiler(expression, start, offset);
        if(ctx != null) {
            c.setPCtx(ctx);
        }

        return _optimizeTree(c._compile());
    }

    public static Serializable subCompileExpression(String expression, ParserContext ctx) {
        ExpressionCompiler c = new ExpressionCompiler(expression);
        c.setPCtx(ctx);
        return _optimizeTree(c._compile());
    }

    public static Serializable optimizeTree(CompiledExpression compiled) {
        return (Serializable)(!compiled.isImportInjectionRequired() && compiled.getParserContext().isAllowBootstrapBypass() && compiled.isSingleNode()?_optimizeTree(compiled):compiled);
    }

    private static Serializable _optimizeTree(CompiledExpression compiled) {
        if(compiled.isSingleNode()) {
            ASTNode tk = compiled.getFirstNode();
            return (Serializable)(tk.isLiteral() && !tk.isThisVal()?new ExecutableLiteral(tk.getLiteralValue()):(tk.canSerializeAccessor()?new ExecutableAccessorSafe(tk, compiled.getKnownEgressType()):new ExecutableAccessor(tk, compiled.getKnownEgressType())));
        } else {
            return compiled;
        }
    }

    public static boolean isWhitespace(char c) {
        return c < 33;
    }

    public static String repeatChar(char c, int times) {
        char[] n = new char[times];

        for(int i = 0; i < times; ++i) {
            n[i] = c;
        }

        return new String(n);
    }

    public static char[] loadFromFile(File file) throws IOException {
        return loadFromFile(file, (String)null);
    }

    public static char[] loadFromFile(File file, String encoding) throws IOException {
        if(!file.exists()) {
            throw new RuntimeException("cannot find file: " + file.getName());
        } else {
            FileInputStream inStream = null;
            FileChannel fc = null;

            try {
                fc = (inStream = new FileInputStream(file)).getChannel();
                ByteBuffer e = ByteBuffer.allocateDirect(10);
                StringAppender sb = new StringAppender((int)file.length(), encoding);
                int read = 0;

                label102:
                while(true) {
                    if(read >= 0) {
                        e.rewind();
                        read = fc.read(e);
                        e.rewind();

                        while(true) {
                            if(read <= 0) {
                                continue label102;
                            }

                            sb.append(e.get());
                            --read;
                        }
                    }

                    char[] var7 = sb.toChars();
                    return var7;
                }
            } catch (FileNotFoundException var11) {
                ;
            } finally {
                if(inStream != null) {
                    inStream.close();
                }

                if(fc != null) {
                    fc.close();
                }

            }

            return null;
        }
    }

    public static char[] readIn(InputStream inStream, String encoding) throws IOException {
        try {
            byte[] buf = new byte[10];
            StringAppender sb = new StringAppender(10, encoding);

            label62:
            while(true) {
                int bytesRead;
                if((bytesRead = inStream.read(buf)) > 0) {
                    int var9 = 0;

                    while(true) {
                        if(var9 >= bytesRead) {
                            continue label62;
                        }

                        sb.append(buf[var9]);
                        ++var9;
                    }
                }

                char[] i = sb.toChars();
                return i;
            }
        } finally {
            if(inStream != null) {
                inStream.close();
            }

        }
    }

    static {
//        try {
//            double t = Double.parseDouble(System.getProperty("java.version").substring(0, 3));
//            if(t < 1.5D) {
//                throw new RuntimeException("unsupported java version: " + t);
//            }
//        } catch (RuntimeException var2) {
//            throw var2;
//        } catch (Exception var3) {
//            throw new RuntimeException("unable to initialize math processor", var3);
//        }

        RESOLVED_METH_CACHE = new WeakHashMap(10);
        RESOLVED_CONST_CACHE = new WeakHashMap(10);
        CONSTRUCTOR_PARMS_CACHE = new WeakHashMap(10);
        CLASS_RESOLVER_CACHE = new WeakHashMap(1, 1.0F);
        CLASS_CONSTRUCTOR_CACHE = new WeakHashMap(10);
        typeResolveMap = new HashMap();
        HashMap t1 = typeResolveMap;
        t1.put(BigDecimal.class, Integer.valueOf(110));
        t1.put(BigInteger.class, Integer.valueOf(111));
        t1.put(String.class, Integer.valueOf(1));
        t1.put(Integer.TYPE, Integer.valueOf(101));
        t1.put(Integer.class, Integer.valueOf(106));
        t1.put(Short.TYPE, Integer.valueOf(100));
        t1.put(Short.class, Integer.valueOf(105));
        t1.put(Float.TYPE, Integer.valueOf(104));
        t1.put(Float.class, Integer.valueOf(108));
        t1.put(Double.TYPE, Integer.valueOf(103));
        t1.put(Double.class, Integer.valueOf(109));
        t1.put(Long.TYPE, Integer.valueOf(102));
        t1.put(Long.class, Integer.valueOf(107));
        t1.put(Boolean.TYPE, Integer.valueOf(7));
        t1.put(Boolean.class, Integer.valueOf(15));
        t1.put(Byte.TYPE, Integer.valueOf(9));
        t1.put(Byte.class, Integer.valueOf(113));
        t1.put(Character.TYPE, Integer.valueOf(8));
        t1.put(Character.class, Integer.valueOf(112));
        t1.put(BlankLiteral.class, Integer.valueOf(200));
        typeCodes = new HashMap(30, 0.5F);
        typeCodes.put(Integer.class, Integer.valueOf(106));
        typeCodes.put(Double.class, Integer.valueOf(109));
        typeCodes.put(Boolean.class, Integer.valueOf(15));
        typeCodes.put(String.class, Integer.valueOf(1));
        typeCodes.put(Long.class, Integer.valueOf(107));
        typeCodes.put(Short.class, Integer.valueOf(105));
        typeCodes.put(Float.class, Integer.valueOf(108));
        typeCodes.put(Byte.class, Integer.valueOf(113));
        typeCodes.put(Character.class, Integer.valueOf(112));
        typeCodes.put(BigDecimal.class, Integer.valueOf(110));
        typeCodes.put(BigInteger.class, Integer.valueOf(111));
        typeCodes.put(Integer.TYPE, Integer.valueOf(101));
        typeCodes.put(Double.TYPE, Integer.valueOf(103));
        typeCodes.put(Boolean.TYPE, Integer.valueOf(7));
        typeCodes.put(Long.TYPE, Integer.valueOf(102));
        typeCodes.put(Short.TYPE, Integer.valueOf(100));
        typeCodes.put(Float.TYPE, Integer.valueOf(104));
        typeCodes.put(Byte.TYPE, Integer.valueOf(9));
        typeCodes.put(Character.TYPE, Integer.valueOf(8));
        typeCodes.put(BlankLiteral.class, Integer.valueOf(200));
    }
}
