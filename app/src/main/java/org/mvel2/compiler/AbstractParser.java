//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.mvel2.compiler;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.WeakHashMap;
import java.util.regex.Pattern;
import org.mvel2.CompileException;
import org.mvel2.DataConversion;
import org.mvel2.ErrorDetail;
import org.mvel2.Operator;
import org.mvel2.ParserContext;
import org.mvel2.ast.ASTNode;
import org.mvel2.ast.AssertNode;
import org.mvel2.ast.AssignmentNode;
import org.mvel2.ast.BooleanNode;
import org.mvel2.ast.DeclProtoVarNode;
import org.mvel2.ast.DeclTypedVarNode;
import org.mvel2.ast.DeepAssignmentNode;
import org.mvel2.ast.DoNode;
import org.mvel2.ast.DoUntilNode;
import org.mvel2.ast.EndOfStatement;
import org.mvel2.ast.Fold;
import org.mvel2.ast.ForEachNode;
import org.mvel2.ast.ForNode;
import org.mvel2.ast.Function;
import org.mvel2.ast.IfNode;
import org.mvel2.ast.ImportNode;
import org.mvel2.ast.IndexedAssignmentNode;
import org.mvel2.ast.IndexedDeclTypedVarNode;
import org.mvel2.ast.IndexedOperativeAssign;
import org.mvel2.ast.IndexedPostFixDecNode;
import org.mvel2.ast.IndexedPostFixIncNode;
import org.mvel2.ast.IndexedPreFixDecNode;
import org.mvel2.ast.IndexedPreFixIncNode;
import org.mvel2.ast.InlineCollectionNode;
import org.mvel2.ast.InterceptorWrapper;
import org.mvel2.ast.Invert;
import org.mvel2.ast.IsDef;
import org.mvel2.ast.LineLabel;
import org.mvel2.ast.LiteralDeepPropertyNode;
import org.mvel2.ast.LiteralNode;
import org.mvel2.ast.Negation;
import org.mvel2.ast.NewObjectNode;
import org.mvel2.ast.NewPrototypeNode;
import org.mvel2.ast.OperativeAssign;
import org.mvel2.ast.OperatorNode;
import org.mvel2.ast.PostFixDecNode;
import org.mvel2.ast.PostFixIncNode;
import org.mvel2.ast.PreFixDecNode;
import org.mvel2.ast.PreFixIncNode;
import org.mvel2.ast.Proto;
import org.mvel2.ast.ProtoVarNode;
import org.mvel2.ast.RedundantCodeException;
import org.mvel2.ast.RegExMatch;
import org.mvel2.ast.ReturnNode;
import org.mvel2.ast.Sign;
import org.mvel2.ast.Stacklang;
import org.mvel2.ast.StaticImportNode;
import org.mvel2.ast.Substatement;
import org.mvel2.ast.ThisWithNode;
import org.mvel2.ast.TypeCast;
import org.mvel2.ast.TypeDescriptor;
import org.mvel2.ast.TypedVarNode;
import org.mvel2.ast.Union;
import org.mvel2.ast.UntilNode;
import org.mvel2.ast.WhileNode;
import org.mvel2.ast.WithNode;
import org.mvel2.compiler.BlankLiteral;
import org.mvel2.compiler.Parser;
import org.mvel2.integration.Interceptor;
import org.mvel2.integration.VariableResolverFactory;
import org.mvel2.util.ArrayTools;
import org.mvel2.util.ErrorUtil;
import org.mvel2.util.ExecutionStack;
import org.mvel2.util.FunctionParser;
import org.mvel2.util.ParseTools;
import org.mvel2.util.PropertyTools;
import org.mvel2.util.ProtoParser;
import org.mvel2.util.Soundex;

public class AbstractParser implements Parser, Serializable {
    protected char[] expr;
    protected int cursor;
    protected int start;
    protected int length;
    protected int end;
    protected int st;
    protected int fields;
    protected static final int OP_OVERFLOW = -2;
    protected static final int OP_TERMINATE = -1;
    protected static final int OP_RESET_FRAME = 0;
    protected static final int OP_CONTINUE = 1;
    protected boolean greedy = true;
    protected boolean lastWasIdentifier = false;
    protected boolean lastWasLineLabel = false;
    protected boolean lastWasComment = false;
    protected boolean compileMode = false;
    protected int literalOnly = -1;
    protected int lastLineStart = 0;
    protected int line = 0;
    protected ASTNode lastNode;
    private static final WeakHashMap<String, char[]> EX_PRECACHE = new WeakHashMap(15);
    public static HashMap<String, Object> LITERALS;
    public static HashMap<String, Object> CLASS_LITERALS;
    public static HashMap<String, Integer> OPERATORS;
    protected ExecutionStack stk;
    protected ExecutionStack splitAccumulator = new ExecutionStack();
    protected static ThreadLocal<ParserContext> parserContext;
    protected ParserContext pCtx;
    protected ExecutionStack dStack;
    protected Object ctx;
    protected VariableResolverFactory variableFactory;
    protected boolean debugSymbols = false;
    protected static final int SET = 0;
    protected static final int REMOVE = 1;
    protected static final int GET = 2;
    protected static final int GET_OR_CREATE = 3;
    public static final int LEVEL_5_CONTROL_FLOW = 5;
    public static final int LEVEL_4_ASSIGNMENT = 4;
    public static final int LEVEL_3_ITERATION = 3;
    public static final int LEVEL_2_MULTI_STATEMENT = 2;
    public static final int LEVEL_1_BASIC_LANG = 1;
    public static final int LEVEL_0_PROPERTY_ONLY = 0;

    public AbstractParser() {
    }

    public static void setupParser() {
        if(LITERALS == null || LITERALS.isEmpty()) {
            LITERALS = new HashMap();
            CLASS_LITERALS = new HashMap();
            OPERATORS = new HashMap();
            CLASS_LITERALS.put("System", System.class);
            CLASS_LITERALS.put("String", String.class);
            CLASS_LITERALS.put("CharSequence", CharSequence.class);
            CLASS_LITERALS.put("Integer", Integer.class);
            CLASS_LITERALS.put("int", Integer.class);
            CLASS_LITERALS.put("Long", Long.class);
            CLASS_LITERALS.put("long", Long.class);
            CLASS_LITERALS.put("Boolean", Boolean.class);
            CLASS_LITERALS.put("boolean", Boolean.class);
            CLASS_LITERALS.put("Short", Short.class);
            CLASS_LITERALS.put("short", Short.class);
            CLASS_LITERALS.put("Character", Character.class);
            CLASS_LITERALS.put("char", Character.class);
            CLASS_LITERALS.put("Double", Double.class);
            CLASS_LITERALS.put("double", Double.class);
            CLASS_LITERALS.put("Float", Float.class);
            CLASS_LITERALS.put("float", Float.class);
            CLASS_LITERALS.put("Byte", Byte.class);
            CLASS_LITERALS.put("byte", Byte.class);
            CLASS_LITERALS.put("Math", Math.class);
            CLASS_LITERALS.put("Void", Void.class);
            CLASS_LITERALS.put("Object", Object.class);
            CLASS_LITERALS.put("Number", Number.class);
            CLASS_LITERALS.put("Class", Class.class);
            CLASS_LITERALS.put("ClassLoader", ClassLoader.class);
            CLASS_LITERALS.put("Runtime", Runtime.class);
            CLASS_LITERALS.put("Thread", Thread.class);
            CLASS_LITERALS.put("Compiler", Compiler.class);
            CLASS_LITERALS.put("StringBuffer", StringBuffer.class);
            CLASS_LITERALS.put("ThreadLocal", ThreadLocal.class);
            CLASS_LITERALS.put("SecurityManager", SecurityManager.class);
            CLASS_LITERALS.put("StrictMath", StrictMath.class);
            CLASS_LITERALS.put("Exception", Exception.class);
            CLASS_LITERALS.put("Array", Array.class);
//            if(Double.parseDouble(System.getProperty("java.version").substring(0, 3)) >= 1.5D) {
//                try {
//                    CLASS_LITERALS.put("StringBuilder", Thread.currentThread().getContextClassLoader().loadClass("java.lang.StringBuilder"));
//                } catch (Exception var1) {
//                    throw new RuntimeException("cannot resolve a built-in literal", var1);
//                }
//            }

            try {
                    CLASS_LITERALS.put("StringBuilder", Thread.currentThread().getContextClassLoader().loadClass("java.lang.StringBuilder"));
                } catch (Exception var1) {
                    throw new RuntimeException("cannot resolve a built-in literal", var1);
                }

            LITERALS.putAll(CLASS_LITERALS);
            LITERALS.put("true", Boolean.TRUE);
            LITERALS.put("false", Boolean.FALSE);
            LITERALS.put("null", (Object)null);
            LITERALS.put("nil", (Object)null);
            LITERALS.put("empty", BlankLiteral.INSTANCE);
            setLanguageLevel(Boolean.getBoolean("mvel.future.lang.support")?6:5);
        }

    }

    protected ASTNode nextTokenSkipSymbols() {
        ASTNode n = this.nextToken();
        if(n != null && n.getFields() == -1) {
            n = this.nextToken();
        }

        return n;
    }

    protected ASTNode nextToken() {
        try {
            if(!this.splitAccumulator.isEmpty()) {
                this.lastNode = (ASTNode)this.splitAccumulator.pop();
                return this.cursor >= this.end && this.lastNode instanceof EndOfStatement?this.nextToken():this.lastNode;
            } else if(this.cursor >= this.end) {
                return null;
            } else {
                boolean capture = false;
                boolean union = false;
                if((this.fields & 16) != 0 && this.pCtx == null) {
                    this.debugSymbols = (this.pCtx = this.getParserContext()).isDebugSymbols();
                }

                if(this.debugSymbols) {
                    if(!this.lastWasLineLabel) {
                        if(this.pCtx.getSourceFile() == null) {
                            throw new CompileException("unable to produce debugging symbols: source name must be provided.", this.expr, this.st);
                        }

                        if(!this.pCtx.isLineMapped(this.pCtx.getSourceFile())) {
                            this.pCtx.initLineMapping(this.pCtx.getSourceFile(), this.expr);
                        }

                        this.skipWhitespace();
                        if(this.cursor >= this.end) {
                            return null;
                        }

                        int singleToken = this.pCtx.getLineFor(this.pCtx.getSourceFile(), this.cursor);
                        if(!this.pCtx.isVisitedLine(this.pCtx.getSourceFile(), this.pCtx.setLineCount(singleToken)) && !this.pCtx.isBlockSymbols()) {
                            this.lastWasLineLabel = true;
                            this.pCtx.visitLine(this.pCtx.getSourceFile(), singleToken);
                            return this.lastNode = this.pCtx.setLastLineLabel(new LineLabel(this.pCtx.getSourceFile(), singleToken));
                        }
                    } else {
                        this.lastWasComment = this.lastWasLineLabel = false;
                    }
                }

                this.skipWhitespace();
                this.st = this.cursor;

                label1327:
                while(this.cursor != this.end) {
                    if(ParseTools.isIdentifierPart(this.expr[this.cursor])) {
                        capture = true;
                        ++this.cursor;

                        while(this.cursor != this.end && ParseTools.isIdentifierPart(this.expr[this.cursor])) {
                            ++this.cursor;
                        }
                    }

                    int idx;
                    String name;
                    if(capture) {
                        String var22;
                        if(OPERATORS.containsKey(var22 = new String(this.expr, this.st, this.cursor - this.st))) {
                            switch(((Integer)OPERATORS.get(var22)).intValue()) {
                                case 34:
                                    if(!ParseTools.isIdentifierPart(this.expr[this.st = this.cursor = this.trimRight(this.cursor)])) {
                                        throw new CompileException("unexpected character (expected identifier): " + this.expr[this.cursor], this.expr, this.st);
                                    }

                                    do {
                                        this.captureToNextTokenJunction();
                                        this.skipWhitespace();
                                    } while(this.cursor < this.end && this.expr[this.cursor] == 91);

                                    if(this.cursor < this.end && !this.lastNonWhite(']')) {
                                        this.captureToEOT();
                                    }

                                    TypeDescriptor var23 = new TypeDescriptor(this.expr, this.st, this.trimLeft(this.cursor) - this.st, this.fields);
                                    if(this.pCtx == null) {
                                        this.pCtx = this.getParserContext();
                                    }

                                    if(this.pCtx.hasProtoImport(var23.getClassName())) {
                                        return this.lastNode = new NewPrototypeNode(var23);
                                    }

                                    this.lastNode = new NewObjectNode(var23, this.fields, this.pCtx);
                                    this.skipWhitespace();
                                    if(this.cursor != this.end && this.expr[this.cursor] == 123) {
                                        if(!((NewObjectNode)this.lastNode).getTypeDescr().isUndimensionedArray()) {
                                            throw new CompileException("conflicting syntax: dimensioned array with initializer block", this.expr, this.st);
                                        }

                                        this.st = this.cursor;
                                        Class var26 = this.lastNode.getEgressType();
                                        if(var26 == null) {
                                            try {
                                                var26 = TypeDescriptor.getClassReference(this.pCtx, var23);
                                            } catch (ClassNotFoundException var13) {
                                                throw new CompileException("could not instantiate class", this.expr, this.st, var13);
                                            }
                                        }

                                        this.cursor = ParseTools.balancedCaptureWithLineAccounting(this.expr, this.st, this.end, this.expr[this.cursor], this.pCtx) + 1;
                                        if(this.tokenContinues()) {
                                            this.lastNode = new InlineCollectionNode(this.expr, this.st, this.cursor - this.st, this.fields, var26, this.pCtx);
                                            this.st = this.cursor;
                                            this.captureToEOT();
                                            return this.lastNode = new Union(this.expr, this.st + 1, this.cursor, this.fields, this.lastNode);
                                        }

                                        return this.lastNode = new InlineCollectionNode(this.expr, this.st, this.cursor - this.st, this.fields, var26, this.pCtx);
                                    }

                                    if(((NewObjectNode)this.lastNode).getTypeDescr().isUndimensionedArray()) {
                                        throw new CompileException("array initializer expected", this.expr, this.st);
                                    }

                                    this.st = this.cursor;
                                    return this.lastNode;
                                case 35:
                                case 36:
                                case 37:
                                case 44:
                                case 49:
                                case 50:
                                case 51:
                                case 52:
                                case 53:
                                case 54:
                                case 55:
                                case 56:
                                case 57:
                                case 58:
                                case 59:
                                case 60:
                                case 61:
                                case 62:
                                case 63:
                                case 64:
                                case 65:
                                case 66:
                                case 67:
                                case 68:
                                case 69:
                                case 70:
                                case 71:
                                case 72:
                                case 73:
                                case 74:
                                case 75:
                                case 76:
                                case 77:
                                case 78:
                                case 79:
                                case 80:
                                case 81:
                                case 82:
                                case 83:
                                case 84:
                                case 85:
                                case 86:
                                case 87:
                                case 88:
                                case 89:
                                case 90:
                                case 91:
                                case 92:
                                case 93:
                                case 94:
                                default:
                                    break;
                                case 38:
                                    return this.captureCodeBlock(4096);
                                case 39:
                                    return this.captureCodeBlock(2048);
                                case 40:
                                    throw new CompileException("else without if", this.expr, this.st);
                                case 41:
                                    return this.captureCodeBlock('耀');
                                case 42:
                                    return this.captureCodeBlock(16384);
                                case 43:
                                    return this.captureCodeBlock(262144);
                                case 45:
                                    return this.captureCodeBlock(65536);
                                case 46:
                                    return this.captureCodeBlock(8192);
                                case 47:
                                    this.st = this.cursor = this.trimRight(this.cursor);
                                    this.captureToNextTokenJunction();
                                    return this.lastNode = new IsDef(this.expr, this.st, this.cursor - this.st);
                                case 48:
                                    return this.captureCodeBlock(48);
                                case 95:
                                    this.st = this.cursor = this.trimRight(this.cursor);
                                    this.captureToEOS();
                                    return this.lastNode = new StaticImportNode(this.expr, this.st, this.trimLeft(this.cursor) - this.st);
                                case 96:
                                    this.st = this.cursor = this.trimRight(this.cursor);
                                    this.captureToEOS();
                                    ImportNode var25 = new ImportNode(this.expr, this.st, this.cursor - this.st);
                                    if(this.pCtx == null) {
                                        this.pCtx = this.getParserContext();
                                    }

                                    if(var25.isPackageImport()) {
                                        this.pCtx.addPackageImport(var25.getPackageImport());
                                    } else {
                                        this.pCtx.addImport(var25.getImportClass().getSimpleName(), var25.getImportClass());
                                    }

                                    return this.lastNode = var25;
                                case 97:
                                    this.st = this.cursor = this.trimRight(this.cursor);
                                    this.captureToEOS();
                                    return this.lastNode = new AssertNode(this.expr, this.st, this.cursor-- - this.st, this.fields, this.pCtx);
                                case 98:
                                    this.st = this.cursor + 1;

                                    while(true) {
                                        this.captureToEOT();
                                        int var27 = this.cursor;
                                        this.skipWhitespace();
                                        if(this.cursor != var27 && this.expr[this.cursor] == 61) {
                                            if(var27 == (this.cursor = this.st)) {
                                                throw new CompileException("illegal use of reserved word: var", this.expr, this.st);
                                            }
                                            continue label1327;
                                        }

                                        name = new String(this.expr, this.st, var27 - this.st);
                                        if(this.pCtx != null && (idx = this.pCtx.variableIndexOf(name)) != -1) {
                                            this.splitAccumulator.add(this.lastNode = new IndexedDeclTypedVarNode(idx, this.st, var27 - this.st, Object.class));
                                        } else {
                                            this.splitAccumulator.add(this.lastNode = new DeclTypedVarNode(name, this.expr, this.st, var27 - this.st, Object.class, this.fields, this.pCtx));
                                        }

                                        if(this.cursor == this.end || this.expr[this.cursor] != 44) {
                                            return (ASTNode)this.splitAccumulator.pop();
                                        }

                                        ++this.cursor;
                                        this.skipWhitespace();
                                        this.st = this.cursor;
                                    }
                                case 99:
                                    this.st = this.cursor = this.trimRight(this.cursor);
                                    this.captureToEOS();
                                    return this.lastNode = new ReturnNode(this.expr, this.st, this.cursor - this.st, this.fields, this.pCtx);
                                case 100:
                                    this.lastNode = this.captureCodeBlock(100);
                                    this.st = this.cursor + 1;
                                    return this.lastNode;
                                case 101:
                                    return this.captureCodeBlock(101);
                            }
                        }

                        this.skipWhitespace();
                        if(this.cursor != this.end && this.expr[this.cursor] == 40) {
                            this.cursor = ParseTools.balancedCaptureWithLineAccounting(this.expr, this.cursor, this.end, '(', this.pCtx) + 1;
                        }

                        label1293:
                        while(this.cursor != this.end) {
                            switch(this.expr[this.cursor]) {
                                case '!':
                                case '\"':
                                case '\'':
                                case ',':
                                case ':':
                                case ';':
                                    break label1293;
                                case '%':
                                case '&':
                                case '*':
                                case '/':
                                case '^':
                                case '|':
                                case '«':
                                case '¬':
                                case '»':
                                    char var24 = this.expr[this.cursor];
                                    if(this.lookAhead() == 61) {
                                        name = new String(this.expr, this.st, this.trimLeft(this.cursor) - this.st);
                                        this.st = this.cursor += 2;
                                        this.captureToEOS();
                                        if(union) {
                                            return this.lastNode = new DeepAssignmentNode(this.expr, this.st, this.cursor - this.st, this.fields, ParseTools.opLookup(var24), var22, this.pCtx);
                                        }

                                        if(this.pCtx != null && (idx = this.pCtx.variableIndexOf(name)) != -1) {
                                            return this.lastNode = new IndexedOperativeAssign(this.expr, this.st, this.cursor - this.st, ParseTools.opLookup(var24), idx, this.fields, this.pCtx);
                                        }

                                        return this.lastNode = new OperativeAssign(name, this.expr, this.st, this.cursor - this.st, ParseTools.opLookup(var24), this.fields, this.pCtx);
                                    }
                                    break label1293;
                                case '(':
                                    this.cursor = ParseTools.balancedCaptureWithLineAccounting(this.expr, this.cursor, this.end, '(', this.pCtx) + 1;
                                    break;
                                case '+':
                                    switch(this.lookAhead()) {
                                        case '+':
                                            name = new String(this.subArray(this.st, this.trimLeft(this.cursor)));
                                            if(this.pCtx != null && (idx = this.pCtx.variableIndexOf(name)) != -1) {
                                                this.lastNode = new IndexedPostFixIncNode(idx, this.pCtx);
                                            } else {
                                                this.lastNode = new PostFixIncNode(name, this.pCtx);
                                            }

                                            this.cursor += 2;
                                            this.expectEOS();
                                            return this.lastNode;
                                        case '=':
                                            name = ParseTools.createStringTrimmed(this.expr, this.st, this.cursor - this.st);
                                            this.st = this.cursor += 2;
                                            this.captureToEOS();
                                            if(union) {
                                                return this.lastNode = new DeepAssignmentNode(this.expr, this.st = this.trimRight(this.st), this.trimLeft(this.cursor) - this.st, this.fields, 0, name, this.pCtx);
                                            }

                                            if(this.pCtx != null && (idx = this.pCtx.variableIndexOf(name)) != -1) {
                                                return this.lastNode = new IndexedAssignmentNode(this.expr, this.st, this.cursor - this.st, this.fields, 0, name, idx, this.pCtx);
                                            }

                                            return this.lastNode = new OperativeAssign(name, this.expr, this.st = this.trimRight(this.st), this.trimLeft(this.cursor) - this.st, 0, this.fields, this.pCtx);
                                        default:
                                            if(ParseTools.isDigit(this.lookAhead()) && this.cursor > 1 && (this.expr[this.cursor - 1] == 69 || this.expr[this.cursor - 1] == 101) && ParseTools.isDigit(this.expr[this.cursor - 2])) {
                                                ++this.cursor;
                                                continue label1327;
                                            }
                                            break label1293;
                                    }
                                case '-':
                                    switch(this.lookAhead()) {
                                        case '-':
                                            name = new String(this.subArray(this.st, this.trimLeft(this.cursor)));
                                            if(this.pCtx != null && (idx = this.pCtx.variableIndexOf(name)) != -1) {
                                                this.lastNode = new IndexedPostFixDecNode(idx, this.pCtx);
                                            } else {
                                                this.lastNode = new PostFixDecNode(name, this.pCtx);
                                            }

                                            this.cursor += 2;
                                            this.expectEOS();
                                            return this.lastNode;
                                        case '=':
                                            name = new String(this.expr, this.st, this.trimLeft(this.cursor) - this.st);
                                            this.st = this.cursor += 2;
                                            this.captureToEOS();
                                            if(union) {
                                                return this.lastNode = new DeepAssignmentNode(this.expr, this.st, this.cursor - this.st, this.fields, 1, var22, this.pCtx);
                                            }

                                            if(this.pCtx != null && (idx = this.pCtx.variableIndexOf(name)) != -1) {
                                                return this.lastNode = new IndexedOperativeAssign(this.expr, this.st, this.cursor - this.st, 1, idx, this.fields, this.pCtx);
                                            }

                                            return this.lastNode = new OperativeAssign(name, this.expr, this.st, this.cursor - this.st, 1, this.fields, this.pCtx);
                                        default:
                                            if(ParseTools.isDigit(this.lookAhead()) && this.cursor > 1 && (this.expr[this.cursor - 1] == 69 || this.expr[this.cursor - 1] == 101) && ParseTools.isDigit(this.expr[this.cursor - 2])) {
                                                ++this.cursor;
                                                capture = true;
                                                continue label1327;
                                            }
                                            break label1293;
                                    }
                                case '.':
                                    union = true;
                                    ++this.cursor;
                                    this.skipWhitespace();
                                    break;
                                case '<':
                                    if(this.lookAhead() == 60 && this.lookAhead(2) == 61) {
                                        name = new String(this.expr, this.st, this.trimLeft(this.cursor) - this.st);
                                        this.st = this.cursor += 3;
                                        this.captureToEOS();
                                        if(union) {
                                            return this.lastNode = new DeepAssignmentNode(this.expr, this.st, this.cursor - this.st, this.fields, 10, var22, this.pCtx);
                                        }

                                        if(this.pCtx != null && (idx = this.pCtx.variableIndexOf(name)) != -1) {
                                            return this.lastNode = new IndexedOperativeAssign(this.expr, this.st, this.cursor - this.st, 10, idx, this.fields, this.pCtx);
                                        }

                                        return this.lastNode = new OperativeAssign(name, this.expr, this.st, this.cursor - this.st, 10, this.fields, this.pCtx);
                                    }
                                    break label1293;
                                case '=':
                                    if(this.lookAhead() == 43) {
                                        name = new String(this.expr, this.st, this.trimLeft(this.cursor) - this.st);
                                        this.st = this.cursor += 2;
                                        if(!this.isNextIdentifierOrLiteral()) {
                                            throw new CompileException("unexpected symbol \'" + this.expr[this.cursor] + "\'", this.expr, this.st);
                                        }

                                        this.captureToEOS();
                                        if(this.pCtx != null && (idx = this.pCtx.variableIndexOf(name)) != -1) {
                                            return this.lastNode = new IndexedOperativeAssign(this.expr, this.st, this.cursor - this.st, 0, idx, this.fields, this.pCtx);
                                        }

                                        return this.lastNode = new OperativeAssign(name, this.expr, this.st, this.cursor - this.st, 0, this.fields, this.pCtx);
                                    }

                                    if(this.lookAhead() == 45) {
                                        name = new String(this.expr, this.st, this.trimLeft(this.cursor) - this.st);
                                        this.st = this.cursor += 2;
                                        if(!this.isNextIdentifierOrLiteral()) {
                                            throw new CompileException("unexpected symbol \'" + this.expr[this.cursor] + "\'", this.expr, this.st);
                                        }

                                        this.captureToEOS();
                                        if(this.pCtx != null && (idx = this.pCtx.variableIndexOf(name)) != -1) {
                                            return this.lastNode = new IndexedOperativeAssign(this.expr, this.st, this.cursor - this.st, 1, idx, this.fields, this.pCtx);
                                        }

                                        return this.lastNode = new OperativeAssign(name, this.expr, this.st, this.cursor - this.st, 1, this.fields, this.pCtx);
                                    }

                                    if(this.greedy && this.lookAhead() != 61) {
                                        ++this.cursor;
                                        if(union) {
                                            this.captureToEOS();
                                            return this.lastNode = new DeepAssignmentNode(this.expr, this.st, this.cursor - this.st, this.fields | 128, this.pCtx);
                                        }

                                        if(this.lastWasIdentifier) {
                                            return this.procTypedNode(false);
                                        }

                                        if(this.pCtx != null && (idx = this.pCtx.variableIndexOf(var22)) != -1 && this.pCtx.isIndexAllocation()) {
                                            this.captureToEOS();
                                            IndexedAssignmentNode var29 = new IndexedAssignmentNode(this.expr, this.st = this.trimRight(this.st), this.trimLeft(this.cursor) - this.st, 128, idx, this.pCtx);
                                            if(idx == -1) {
                                                this.pCtx.addIndexedInput(var22 = var29.getAssignmentVar());
                                                var29.setRegister(this.pCtx.variableIndexOf(var22));
                                            }

                                            return this.lastNode = var29;
                                        }

                                        this.captureToEOS();
                                        return this.lastNode = new AssignmentNode(this.expr, this.st, this.cursor - this.st, this.fields | 128, this.pCtx);
                                    }
                                    break label1293;
                                case '>':
                                    if(this.lookAhead() == 62) {
                                        if(this.lookAhead(2) == 61) {
                                            name = new String(this.expr, this.st, this.trimLeft(this.cursor) - this.st);
                                            this.st = this.cursor += 3;
                                            this.captureToEOS();
                                            if(union) {
                                                return this.lastNode = new DeepAssignmentNode(this.expr, this.st, this.cursor - this.st, this.fields, 9, var22, this.pCtx);
                                            }

                                            if(this.pCtx != null && (idx = this.pCtx.variableIndexOf(name)) != -1) {
                                                return this.lastNode = new IndexedOperativeAssign(this.expr, this.st, this.cursor - this.st, 9, idx, this.fields, this.pCtx);
                                            }

                                            return this.lastNode = new OperativeAssign(name, this.expr, this.st, this.cursor - this.st, 9, this.fields, this.pCtx);
                                        }

                                        if(this.lookAhead(2) == 62 && this.lookAhead(3) == 61) {
                                            name = new String(this.expr, this.st, this.trimLeft(this.cursor) - this.st);
                                            this.st = this.cursor += 4;
                                            this.captureToEOS();
                                            if(union) {
                                                return this.lastNode = new DeepAssignmentNode(this.expr, this.st, this.cursor - this.st, this.fields, 11, var22, this.pCtx);
                                            }

                                            if(this.pCtx != null && (idx = this.pCtx.variableIndexOf(name)) != -1) {
                                                return this.lastNode = new IndexedOperativeAssign(this.expr, this.st, this.cursor - this.st, 11, idx, this.fields, this.pCtx);
                                            }

                                            return this.lastNode = new OperativeAssign(name, this.expr, this.st, this.cursor - this.st, 11, this.fields, this.pCtx);
                                        }
                                    }
                                    break label1293;
                                case '?':
                                    if(this.lookToLast() != 46) {
                                        break label1293;
                                    }

                                    union = true;
                                    ++this.cursor;
                                    break;
                                case '[':
                                    this.cursor = ParseTools.balancedCaptureWithLineAccounting(this.expr, this.cursor, this.end, '[', this.pCtx) + 1;
                                    break;
                                case '{':
                                    if(!union) {
                                        break label1293;
                                    }

                                    this.cursor = ParseTools.balancedCaptureWithLineAccounting(this.expr, this.cursor, this.end, '{', this.pCtx) + 1;
                                    break;
                                case '~':
                                    if(this.lookAhead() == 61) {
                                        int var20 = this.st;
                                        int var28 = this.cursor - this.st;
                                        this.st = this.cursor += 2;
                                        this.captureToEOT();
                                        return this.lastNode = new RegExMatch(this.expr, var20, var28, this.fields, this.st, this.cursor - this.st, this.pCtx);
                                    }
                                    break label1293;
                                default:
                                    if(this.cursor == this.end) {
                                        break label1293;
                                    }

                                    if(ParseTools.isIdentifierPart(this.expr[this.cursor])) {
                                        if(!union) {
                                            break label1293;
                                        }

                                        ++this.cursor;

                                        while(this.cursor != this.end && ParseTools.isIdentifierPart(this.expr[this.cursor])) {
                                            ++this.cursor;
                                        }
                                    } else {
                                        if(this.cursor + 1 != this.end && ParseTools.isIdentifierPart(this.expr[this.cursor + 1])) {
                                            break label1293;
                                        }

                                        ++this.cursor;
                                    }
                            }
                        }

                        this.trimWhitespace();
                        return this.createPropertyToken(this.st, this.cursor);
                    } else {
                        switch(this.expr[this.cursor]) {
                            case '!':
                                ++this.cursor;
                                if(this.isNextIdentifier()) {
                                    if(this.lastNode != null && !this.lastNode.isOperator()) {
                                        throw new CompileException("unexpected operator \'!\'", this.expr, this.st);
                                    }

                                    this.st = this.cursor;
                                    this.captureToEOT();
                                    if(!"new".equals(name = new String(this.expr, this.st, this.cursor - this.st)) && !"isdef".equals(name)) {
                                        return this.lastNode = new Negation(this.expr, this.st, this.cursor - this.st, this.fields, this.pCtx);
                                    }

                                    this.captureToEOT();
                                    return this.lastNode = new Negation(this.expr, this.st, this.cursor - this.st, this.fields, this.pCtx);
                                }

                                if(this.expr[this.cursor] == 40) {
                                    this.st = this.cursor--;
                                    this.captureToEOT();
                                    return this.lastNode = new Negation(this.expr, this.st, this.cursor - this.st, this.fields, this.pCtx);
                                }

                                if(this.expr[this.cursor] != 61) {
                                    throw new CompileException("unexpected operator \'!\'", this.expr, this.st, (Throwable)null);
                                }

                                return this.createOperator(this.expr, this.st, ++this.cursor);
                            case '\"':
                            case '\'':
                                this.lastNode = new LiteralNode(ParseTools.handleStringEscapes(ParseTools.subset(this.expr, this.st + 1, (this.cursor = ParseTools.captureStringLiteral(this.expr[this.cursor], this.expr, this.cursor, this.end)) - this.st - 1)), String.class);
                                ++this.cursor;
                                if(this.tokenContinues()) {
                                    return this.lastNode = this.handleUnion(this.lastNode);
                                }

                                return this.lastNode;
                            case '#':
                            case '%':
                            case '/':
                            case ':':
                            case '?':
                            case '^':
                                return this.createOperator(this.expr, this.st, this.cursor++ + 1);
                            case '$':
                            case ',':
                            case '0':
                            case '1':
                            case '2':
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                            case '7':
                            case '8':
                            case '9':
                            case 'A':
                            case 'B':
                            case 'C':
                            case 'D':
                            case 'E':
                            case 'F':
                            case 'G':
                            case 'H':
                            case 'I':
                            case 'J':
                            case 'K':
                            case 'L':
                            case 'M':
                            case 'N':
                            case 'O':
                            case 'P':
                            case 'Q':
                            case 'R':
                            case 'S':
                            case 'T':
                            case 'U':
                            case 'V':
                            case 'W':
                            case 'X':
                            case 'Y':
                            case 'Z':
                            case '\\':
                            case '_':
                            case '`':
                            case 'a':
                            case 'b':
                            case 'c':
                            case 'd':
                            case 'e':
                            case 'f':
                            case 'g':
                            case 'h':
                            case 'i':
                            case 'j':
                            case 'k':
                            case 'l':
                            case 'm':
                            case 'n':
                            case 'o':
                            case 'p':
                            case 'q':
                            case 'r':
                            case 's':
                            case 't':
                            case 'u':
                            case 'v':
                            case 'w':
                            case 'x':
                            case 'y':
                            case 'z':
                            default:
                                ++this.cursor;
                                break;
                            case '&':
                                if(this.expr[this.cursor++ + 1] == 38) {
                                    return this.createOperator(this.expr, this.st, ++this.cursor);
                                }

                                return this.createOperator(this.expr, this.st, this.cursor);
                            case '(':
                                ++this.cursor;
                                boolean var21 = true;
                                this.skipWhitespace();

                                int e;
                                int _st;
                                for(e = 1; this.cursor != this.end && e != 0; ++this.cursor) {
                                    switch(this.expr[this.cursor]) {
                                        case '\"':
                                            this.cursor = ParseTools.captureStringLiteral('\"', this.expr, this.cursor, this.end);
                                            break;
                                        case '\'':
                                            this.cursor = ParseTools.captureStringLiteral('\'', this.expr, this.cursor, this.end);
                                            break;
                                        case '(':
                                            ++e;
                                            break;
                                        case ')':
                                            --e;
                                            break;
                                        case 'i':
                                            if(e == 1 && ParseTools.isWhitespace(this.lookBehind()) && this.lookAhead() == 110 && ParseTools.isWhitespace(this.lookAhead(2))) {
                                                for(_st = e; this.cursor != this.end; ++this.cursor) {
                                                    switch(this.expr[this.cursor]) {
                                                        case '\"':
                                                            this.cursor = ParseTools.captureStringLiteral('\"', this.expr, this.cursor, this.end);
                                                        case '#':
                                                        case '$':
                                                        case '%':
                                                        case '&':
                                                        default:
                                                            break;
                                                        case '\'':
                                                            this.cursor = ParseTools.captureStringLiteral('\'', this.expr, this.cursor, this.end);
                                                            break;
                                                        case '(':
                                                            ++e;
                                                            break;
                                                        case ')':
                                                            --e;
                                                            if(e < _st) {
                                                                ++this.cursor;
                                                                if(this.tokenContinues()) {
                                                                    this.lastNode = new Fold(this.expr, this.trimRight(this.st + 1), this.cursor - this.st - 2, this.fields, this.pCtx);
                                                                    if(this.expr[this.st = this.cursor] == 46) {
                                                                        ++this.st;
                                                                    }

                                                                    this.captureToEOT();
                                                                    return this.lastNode = new Union(this.expr, this.st = this.trimRight(this.st), this.cursor - this.st, this.fields, this.lastNode);
                                                                }

                                                                return this.lastNode = new Fold(this.expr, this.trimRight(this.st + 1), this.cursor - this.st - 2, this.fields, this.pCtx);
                                                            }
                                                    }
                                                }

                                                throw new CompileException("unterminated projection; closing parathesis required", this.expr, this.st);
                                            }
                                            break;
                                        default:
                                            if(this.expr[this.cursor] != 46) {
                                                switch(this.expr[this.cursor]) {
                                                    case '[':
                                                    case ']':
                                                        break;
                                                    default:
                                                        if(!ParseTools.isIdentifierPart(this.expr[this.cursor]) && this.expr[this.cursor] != 46) {
                                                            var21 = false;
                                                        }
                                                }
                                            }
                                    }
                                }

                                if(e != 0) {
                                    throw new CompileException("unbalanced braces in expression: (" + e + "):", this.expr, this.st);
                                }

                                byte tmpStart = -1;
                                if(var21) {
                                    TypeDescriptor tDescr = new TypeDescriptor(this.expr, _st = this.trimRight(this.st + 1), this.trimLeft(this.cursor - 1) - _st, this.fields);

                                    try {
                                        Class cls;
                                        if(tDescr.isClass() && (cls = TypeDescriptor.getClassReference(this.pCtx, tDescr)) != null) {
                                            boolean e1 = false;

                                            for(int i = this.cursor; i < this.expr.length; ++i) {
                                                if(this.expr[i] != 32 && this.expr[i] != 9) {
                                                    e1 = ParseTools.isIdentifierPart(this.expr[i]) || this.expr[i] == 39 || this.expr[i] == 34 || this.expr[i] == 40;
                                                    break;
                                                }
                                            }

                                            if(e1) {
                                                this.st = this.cursor;
                                                this.captureToEOS();
                                                return this.lastNode = new TypeCast(this.expr, this.st, this.cursor - this.st, cls, this.fields, this.pCtx);
                                            }
                                        }
                                    } catch (ClassNotFoundException var14) {
                                        ;
                                    }
                                }

                                if(tmpStart != -1) {
                                    return this.handleUnion(this.handleSubstatement(new Substatement(this.expr, tmpStart, this.cursor - tmpStart, this.fields, this.pCtx)));
                                }

                                return this.handleUnion(this.handleSubstatement(new Substatement(this.expr, this.st = this.trimRight(this.st + 1), this.trimLeft(this.cursor - 1) - this.st, this.fields, this.pCtx)));
                            case ')':
                            case ']':
                            case '}':
                                throw new CompileException("unbalanced braces", this.expr, this.st);
                            case '*':
                                if(this.lookAhead() == 42) {
                                    ++this.cursor;
                                }

                                return this.createOperator(this.expr, this.st, this.cursor++ + 1);
                            case '+':
                                if(this.lookAhead() == 43) {
                                    this.cursor += 2;
                                    this.skipWhitespace();
                                    this.st = this.cursor;
                                    this.captureIdentifier();
                                    name = new String(this.subArray(this.st, this.cursor));
                                    if(this.pCtx != null && (idx = this.pCtx.variableIndexOf(name)) != -1) {
                                        return this.lastNode = new IndexedPreFixIncNode(idx, this.pCtx);
                                    }

                                    return this.lastNode = new PreFixIncNode(name, this.pCtx);
                                }

                                return this.createOperator(this.expr, this.st, this.cursor++ + 1);
                            case '-':
                                if(this.lookAhead() == 45) {
                                    this.cursor += 2;
                                    this.skipWhitespace();
                                    this.st = this.cursor;
                                    this.captureIdentifier();
                                    name = new String(this.subArray(this.st, this.cursor));
                                    if(this.pCtx != null && (idx = this.pCtx.variableIndexOf(name)) != -1) {
                                        return this.lastNode = new IndexedPreFixDecNode(idx, this.pCtx);
                                    }

                                    return this.lastNode = new PreFixDecNode(name, this.pCtx);
                                }

                                if((this.cursor == this.start || this.lastNode != null && (this.lastNode instanceof BooleanNode || this.lastNode.isOperator())) && !ParseTools.isDigit(this.lookAhead())) {
                                    this.captureToEOT();
                                    return new Sign(this.expr, this.st, this.cursor - this.st, this.fields, this.pCtx);
                                }

                                if((this.cursor == this.start || ParseTools.isWhitespace(this.expr[this.cursor - 1]) || this.lastNode != null && (this.lastNode instanceof BooleanNode || this.lastNode.isOperator())) && ParseTools.isDigit(this.lookAhead())) {
                                    if(this.cursor - 1 == this.start && (ParseTools.isDigit(this.expr[this.cursor - 1]) || !ParseTools.isDigit(this.lookAhead()))) {
                                        throw new CompileException("not a statement", this.expr, this.st);
                                    }

                                    ++this.cursor;
                                    break;
                                }

                                return this.createOperator(this.expr, this.st, this.cursor++ + 1);
                            case '.':
                                ++this.cursor;
                                if(!ParseTools.isDigit(this.expr[this.cursor])) {
                                    this.expectNextChar_IW('{');
                                    return this.lastNode = new ThisWithNode(this.expr, this.st, this.cursor - this.st - 1, this.cursor + 1, (this.cursor = ParseTools.balancedCaptureWithLineAccounting(this.expr, this.cursor, this.end, '{', this.pCtx) + 1) - 3, this.fields, this.pCtx);
                                }

                                capture = true;
                                break;
                            case ';':
                                ++this.cursor;
                                this.lastWasIdentifier = false;
                                return this.lastNode = new EndOfStatement();
                            case '<':
                                if(this.expr[++this.cursor] == 60) {
                                    if(this.expr[++this.cursor] == 60) {
                                        ++this.cursor;
                                    }

                                    return this.createOperator(this.expr, this.st, this.cursor);
                                }

                                if(this.expr[this.cursor] == 61) {
                                    return this.createOperator(this.expr, this.st, ++this.cursor);
                                }

                                return this.createOperator(this.expr, this.st, this.cursor);
                            case '=':
                                return this.createOperator(this.expr, this.st, this.cursor += 2);
                            case '>':
                                switch(this.expr[this.cursor + 1]) {
                                    case '=':
                                        return this.createOperator(this.expr, this.st, this.cursor += 2);
                                    case '>':
                                        if(this.expr[this.cursor += 2] == 62) {
                                            ++this.cursor;
                                        }

                                        return this.createOperator(this.expr, this.st, this.cursor);
                                    default:
                                        return this.createOperator(this.expr, this.st, ++this.cursor);
                                }
                            case '@':
                                ++this.st;
                                this.captureToEOT();
                                if(this.pCtx != null && this.pCtx.getInterceptors() != null && this.pCtx.getInterceptors().containsKey(name = new String(this.expr, this.st, this.cursor - this.st))) {
                                    return this.lastNode = new InterceptorWrapper((Interceptor)this.pCtx.getInterceptors().get(name), this.nextToken());
                                }

                                throw new CompileException("reference to undefined interceptor: " + new String(this.expr, this.st, this.cursor - this.st), this.expr, this.st);
                            case '[':
                            case '{':
                                this.cursor = ParseTools.balancedCaptureWithLineAccounting(this.expr, this.cursor, this.end, this.expr[this.cursor], this.pCtx) + 1;
                                if(this.tokenContinues()) {
                                    this.lastNode = new InlineCollectionNode(this.expr, this.st, this.cursor - this.st, this.fields, this.pCtx);
                                    this.st = this.cursor;
                                    this.captureToEOT();
                                    if(this.expr[this.st] == 46) {
                                        ++this.st;
                                    }

                                    return this.lastNode = new Union(this.expr, this.st, this.cursor - this.st, this.fields, this.lastNode);
                                }

                                return this.lastNode = new InlineCollectionNode(this.expr, this.st, this.cursor - this.st, this.fields, this.pCtx);
                            case '|':
                                if(this.expr[this.cursor++ + 1] == 124) {
                                    return this.createOperator(this.expr, this.st, ++this.cursor);
                                }

                                return this.createOperator(this.expr, this.st, this.cursor);
                            case '~':
                                if((this.cursor++ - 1 != 0 || !ParseTools.isIdentifierPart(this.lookBehind())) && ParseTools.isDigit(this.expr[this.cursor])) {
                                    this.st = this.cursor;
                                    this.captureToEOT();
                                    return this.lastNode = new Invert(this.expr, this.st, this.cursor - this.st, this.fields, this.pCtx);
                                }

                                if(this.expr[this.cursor] == 40) {
                                    this.st = this.cursor--;
                                    this.captureToEOT();
                                    return this.lastNode = new Invert(this.expr, this.st, this.cursor - this.st, this.fields, this.pCtx);
                                }

                                if(this.expr[this.cursor] == 61) {
                                    ++this.cursor;
                                }

                                return this.createOperator(this.expr, this.st, this.cursor);
                        }
                    }
                }

                if(this.st == this.cursor) {
                    return null;
                } else {
                    return this.createPropertyToken(this.st, this.cursor);
                }
            }
        } catch (RedundantCodeException var15) {
            return this.nextToken();
        } catch (NumberFormatException var16) {
            throw new CompileException("badly formatted number: " + var16.getMessage(), this.expr, this.st, var16);
        } catch (StringIndexOutOfBoundsException var17) {
            throw new CompileException("unexpected end of statement", this.expr, this.cursor, var17);
        } catch (ArrayIndexOutOfBoundsException var18) {
            throw new CompileException("unexpected end of statement", this.expr, this.cursor, var18);
        } catch (CompileException var19) {
            throw ErrorUtil.rewriteIfNeeded(var19, this.expr, this.cursor);
        }
    }

    public ASTNode handleSubstatement(Substatement stmt) {
        return (ASTNode)(stmt.getStatement() != null && stmt.getStatement().isLiteralOnly()?new LiteralNode(stmt.getStatement().getValue((Object)null, (Object)null, (VariableResolverFactory)null)):stmt);
    }

    protected ASTNode handleUnion(ASTNode node) {
        if(this.cursor != this.end) {
            this.skipWhitespace();
            int union = -1;
            if(this.cursor < this.end) {
                switch(this.expr[this.cursor]) {
                    case '.':
                        union = this.cursor + 1;
                        break;
                    case '[':
                        union = this.cursor;
                }
            }

            if(union != -1) {
                this.captureToEOT();
                return this.lastNode = new Union(this.expr, union, this.cursor - union, this.fields, node);
            }
        }

        return this.lastNode = node;
    }

    private ASTNode createOperator(char[] expr, int start, int end) {
        this.lastWasIdentifier = false;
        return this.lastNode = new OperatorNode((Integer)OPERATORS.get(new String(expr, start, end - start)), expr, start);
    }

    private char[] subArray(int start, int end) {
        if(start >= end) {
            return new char[0];
        } else {
            char[] newA = new char[end - start];

            for(int i = 0; i != newA.length; ++i) {
                newA[i] = this.expr[i + start];
            }

            return newA;
        }
    }

    private ASTNode createPropertyToken(int st, int end) {
        if(ParseTools.isPropertyOnly(this.expr, st, end)) {
            String tmp;
            if(this.pCtx != null && this.pCtx.hasImports()) {
                int find;
                if((find = ArrayTools.findFirst('.', st, end - st, this.expr)) != -1) {
                    String iStr = new String(this.expr, st, find - st);
                    if(this.pCtx.hasImport(iStr)) {
                        this.lastWasIdentifier = true;
                        return this.lastNode = new LiteralDeepPropertyNode(this.expr, find + 1, end - find - 1, this.fields, this.pCtx.getImport(iStr));
                    }
                } else if(this.pCtx.hasImport(tmp = new String(this.expr, st, this.cursor - st))) {
                    this.lastWasIdentifier = true;
                    return this.lastNode = new LiteralNode(this.pCtx.getStaticOrClassImport(tmp));
                }
            }

            if(LITERALS.containsKey(tmp = new String(this.expr, st, end - st))) {
                this.lastWasIdentifier = true;
                return this.lastNode = new LiteralNode(LITERALS.get(tmp));
            }

            if(OPERATORS.containsKey(tmp)) {
                this.lastWasIdentifier = false;
                return this.lastNode = new OperatorNode((Integer)OPERATORS.get(tmp), this.expr, st);
            }

            if(this.lastWasIdentifier) {
                return this.procTypedNode(true);
            }
        }

        this.lastWasIdentifier = true;
        return this.lastNode = new ASTNode(this.expr, this.trimRight(st), this.trimLeft(end) - st, this.fields);
    }

    private ASTNode procTypedNode(boolean decl) {
        while(true) {
            if(this.lastNode.getLiteralValue() instanceof String) {
                char[] tmp = ((String)this.lastNode.getLiteralValue()).toCharArray();
                TypeDescriptor tDescr = new TypeDescriptor(tmp, 0, tmp.length, 0);

                try {
                    this.lastNode.setLiteralValue(TypeDescriptor.getClassReference(this.pCtx, tDescr));
                    this.lastNode.discard();
                } catch (Exception var5) {
                    ;
                }
            }

            if(this.lastNode.isLiteral() && this.lastNode.getLiteralValue() instanceof Class) {
                this.lastNode.discard();
                this.captureToEOS();
                if(decl) {
                    this.splitAccumulator.add(new DeclTypedVarNode(new String(this.expr, this.st, this.cursor - this.st), this.expr, this.st, this.cursor - this.st, (Class)this.lastNode.getLiteralValue(), this.fields | 128, this.pCtx));
                } else {
                    this.captureToEOS();
                    this.splitAccumulator.add(new TypedVarNode(this.expr, this.st, this.cursor - this.st - 1, this.fields | 128, (Class)this.lastNode.getLiteralValue(), this.pCtx));
                }
            } else if(this.lastNode instanceof Proto) {
                this.captureToEOS();
                if(decl) {
                    this.splitAccumulator.add(new DeclProtoVarNode(new String(this.expr, this.st, this.cursor - this.st), (Proto)this.lastNode, this.fields | 128, this.pCtx));
                } else {
                    this.splitAccumulator.add(new ProtoVarNode(this.expr, this.st, this.cursor - this.st, this.fields | 128, (Proto)this.lastNode, this.pCtx));
                }
            } else {
                if((this.fields & 16) != 0) {
                    throw new CompileException("unknown class or illegal statement: " + this.lastNode.getLiteralValue(), this.expr, this.cursor);
                }

                if(this.stk.peek() instanceof Class) {
                    this.captureToEOS();
                    if(decl) {
                        this.splitAccumulator.add(new DeclTypedVarNode(new String(this.expr, this.st, this.cursor - this.st), this.expr, this.st, this.cursor - this.st, (Class)this.stk.pop(), this.fields | 128, this.pCtx));
                    } else {
                        this.splitAccumulator.add(new TypedVarNode(this.expr, this.st, this.cursor - this.st, this.fields | 128, (Class)this.stk.pop(), this.pCtx));
                    }
                } else {
                    if(!(this.stk.peek() instanceof Proto)) {
                        throw new CompileException("unknown class or illegal statement: " + this.lastNode.getLiteralValue(), this.expr, this.cursor);
                    }

                    this.captureToEOS();
                    if(decl) {
                        this.splitAccumulator.add(new DeclProtoVarNode(new String(this.expr, this.st, this.cursor - this.st), (Proto)this.stk.pop(), this.fields | 128, this.pCtx));
                    } else {
                        this.splitAccumulator.add(new ProtoVarNode(this.expr, this.st, this.cursor - this.st, this.fields | 128, (Proto)this.stk.pop(), this.pCtx));
                    }
                }
            }

            this.skipWhitespace();
            if(this.cursor >= this.end || this.expr[this.cursor] != 44) {
                return (ASTNode)this.splitAccumulator.pop();
            }

            this.st = ++this.cursor;
            this.splitAccumulator.add(new EndOfStatement());
        }
    }

    private ASTNode createBlockToken(int condStart, int condEnd, int blockStart, int blockEnd, int type) {
        this.lastWasIdentifier = false;
        ++this.cursor;
        if(this.isStatementNotManuallyTerminated()) {
            this.splitAccumulator.add(new EndOfStatement());
        }

        int condOffset = condEnd - condStart;
        int blockOffset = blockEnd - blockStart;
        if(blockOffset < 0) {
            blockOffset = 0;
        }

        switch(type) {
            case 2048:
                return new IfNode(this.expr, condStart, condOffset, blockStart, blockOffset, this.fields, this.pCtx);
            case 16384:
                return new UntilNode(this.expr, condStart, condOffset, blockStart, blockOffset, this.fields, this.pCtx);
            case 32768:
                return new WhileNode(this.expr, condStart, condOffset, blockStart, blockOffset, this.fields, this.pCtx);
            case 65536:
                return new DoNode(this.expr, condStart, condOffset, blockStart, blockOffset, this.fields, this.pCtx);
            case 131072:
                return new DoUntilNode(this.expr, condStart, condOffset, blockStart, blockOffset, this.pCtx);
            case 262144:
                for(int i = condStart; i < condEnd; ++i) {
                    if(this.expr[i] == 59) {
                        return new ForNode(this.expr, condStart, condOffset, blockStart, blockOffset, this.fields, this.pCtx);
                    }

                    if(this.expr[i] == 58) {
                        break;
                    }
                }
            case 4096:
                return new ForEachNode(this.expr, condStart, condOffset, blockStart, blockOffset, this.fields, this.pCtx);
            default:
                return new WithNode(this.expr, condStart, condOffset, blockStart, blockOffset, this.fields, this.pCtx);
        }
    }

    private ASTNode captureCodeBlock(int type) {
        boolean cond = true;
        ASTNode first = null;
        ASTNode tk = null;
        switch(type) {
            case 2048:
                do {
                    if(tk != null) {
                        this.captureToNextTokenJunction();
                        this.skipWhitespace();
                        cond = this.expr[this.cursor] != 123 && this.expr[this.cursor] == 105 && this.expr[++this.cursor] == 102 && this.expr[this.cursor = this.incNextNonBlank()] == 40;
                    }

                    if(((IfNode)(tk = this._captureBlock(tk, this.expr, cond, type))).getElseBlock() != null) {
                        ++this.cursor;
                        return first;
                    }

                    if(first == null) {
                        first = tk;
                    }

                    if(this.cursor != this.end && this.expr[this.cursor] != 59) {
                        ++this.cursor;
                    }
                } while(this.ifThenElseBlockContinues());

                return first;
            case 65536:
                this.skipWhitespace();
                return this._captureBlock((ASTNode)null, this.expr, false, type);
            default:
                this.captureToNextTokenJunction();
                this.skipWhitespace();
                return this._captureBlock((ASTNode)null, this.expr, true, type);
        }
    }

    private ASTNode _captureBlock(ASTNode node, char[] expr, boolean cond, int type) {
        this.skipWhitespace();
        int startCond = 0;
        int endCond = 0;
        String name;
        int ifNode;
        switch(type) {
            case 48:
                if(ProtoParser.isUnresolvedWaiting()) {
                    if(this.pCtx == null) {
                        this.pCtx = this.getParserContext();
                    }

                    ProtoParser.checkForPossibleUnresolvedViolations(expr, this.cursor, this.pCtx);
                }

                ifNode = this.cursor;
                this.captureToNextTokenJunction();
                if(!ParseTools.isReservedWord(name = ParseTools.createStringTrimmed(expr, ifNode, this.cursor - ifNode)) && !ParseTools.isNotValidNameorLabel(name)) {
                    if(expr[this.cursor = this.nextNonBlank()] != 123) {
                        throw new CompileException("expected \'{\' but found: " + expr[this.cursor], expr, this.cursor);
                    }

                    this.cursor = ParseTools.balancedCaptureWithLineAccounting(expr, ifNode = this.cursor + 1, this.end, '{', this.pCtx);
                    if(this.pCtx == null) {
                        this.pCtx = this.getParserContext();
                    }

                    ProtoParser var15 = new ProtoParser(expr, ifNode, this.cursor, name, this.pCtx, this.fields, this.splitAccumulator);
                    Proto var16 = var15.parse();
                    if(this.pCtx == null) {
                        this.pCtx = this.getParserContext();
                    }

                    this.pCtx.addImport(var16);
                    var16.setCursorPosition(ifNode, this.cursor);
                    this.cursor = var15.getCursor();
                    ProtoParser.notifyForLateResolution(var16);
                    return this.lastNode = var16;
                }

                throw new CompileException("illegal prototype name or use of reserved word", expr, this.cursor);
            case 100:
                ifNode = this.cursor;
                this.captureToNextTokenJunction();
                if(this.cursor == this.end) {
                    throw new CompileException("unexpected end of statement", expr, ifNode);
                } else {
                    if(!ParseTools.isReservedWord(name = ParseTools.createStringTrimmed(expr, ifNode, this.cursor - ifNode)) && !ParseTools.isNotValidNameorLabel(name)) {
                        if(this.pCtx == null) {
                            this.pCtx = this.getParserContext();
                        }

                        FunctionParser var13 = new FunctionParser(name, this.cursor, this.end - this.cursor, expr, this.fields, this.pCtx, this.splitAccumulator);
                        Function proto = var13.parse();
                        this.cursor = var13.getCursor();
                        return this.lastNode = proto;
                    }

                    throw new CompileException("illegal function name or use of reserved word", expr, this.cursor);
                }
            case 101:
                if(expr[this.cursor = this.nextNonBlank()] != 123) {
                    throw new CompileException("expected \'{\' but found: " + expr[this.cursor], expr, this.cursor);
                }

                this.cursor = ParseTools.balancedCaptureWithLineAccounting(expr, ifNode = this.cursor + 1, this.end, '{', this.pCtx);
                if(this.pCtx == null) {
                    this.pCtx = this.getParserContext();
                }

                Stacklang stacklang = new Stacklang(expr, ifNode, this.cursor - ifNode, this.fields, this.pCtx);
                ++this.cursor;
                return this.lastNode = stacklang;
            default:
                if(cond) {
                    if(expr[this.cursor] != 40) {
                        throw new CompileException("expected \'(\' but encountered: " + expr[this.cursor], expr, this.cursor);
                    }

                    startCond = this.cursor;
                    endCond = this.cursor = ParseTools.balancedCaptureWithLineAccounting(expr, this.cursor, this.end, '(', this.pCtx);
                    ++startCond;
                    ++this.cursor;
                }

                this.skipWhitespace();
                if(this.cursor >= this.end) {
                    throw new CompileException("unexpected end of statement", expr, this.end);
                } else {
                    int blockStart;
                    int blockEnd;
                    if(expr[this.cursor] == 123) {
                        blockStart = this.cursor;
                        blockEnd = this.cursor = ParseTools.balancedCaptureWithLineAccounting(expr, this.cursor, this.end, '{', this.pCtx);
                    } else {
                        blockStart = this.cursor - 1;
                        this.captureToEOSorEOL();
                        blockEnd = this.cursor + 1;
                    }

                    if(type == 2048) {
                        IfNode var14 = (IfNode)node;
                        return (ASTNode)(node != null?(!cond?var14.setElseBlock(expr, this.st = this.trimRight(blockStart + 1), this.trimLeft(blockEnd) - this.st, this.pCtx):var14.setElseIf((IfNode)this.createBlockToken(startCond, endCond, this.trimRight(blockStart + 1), this.trimLeft(blockEnd), type))):this.createBlockToken(startCond, endCond, blockStart + 1, blockEnd, type));
                    } else if(type == 65536) {
                        ++this.cursor;
                        this.skipWhitespace();
                        this.st = this.cursor;
                        this.captureToNextTokenJunction();
                        if("while".equals(name = new String(expr, this.st, this.cursor - this.st))) {
                            this.skipWhitespace();
                            startCond = this.cursor + 1;
                            endCond = this.cursor = ParseTools.balancedCaptureWithLineAccounting(expr, this.cursor, this.end, '(', this.pCtx);
                            return this.createBlockToken(startCond, endCond, this.trimRight(blockStart + 1), this.trimLeft(blockEnd), type);
                        } else if("until".equals(name)) {
                            this.skipWhitespace();
                            startCond = this.cursor + 1;
                            endCond = this.cursor = ParseTools.balancedCaptureWithLineAccounting(expr, this.cursor, this.end, '(', this.pCtx);
                            return this.createBlockToken(startCond, endCond, this.trimRight(blockStart + 1), this.trimLeft(blockEnd), 131072);
                        } else {
                            throw new CompileException("expected \'while\' or \'until\' but encountered: " + name, expr, this.cursor);
                        }
                    } else {
                        return this.createBlockToken(startCond, endCond, this.trimRight(blockStart + 1), this.trimLeft(blockEnd), type);
                    }
                }
        }
    }

    protected boolean ifThenElseBlockContinues() {
        if(this.cursor + 4 < this.end) {
            if(this.expr[this.cursor] != 59) {
                --this.cursor;
            }

            this.skipWhitespace();
            return this.expr[this.cursor] == 101 && this.expr[this.cursor + 1] == 108 && this.expr[this.cursor + 2] == 115 && this.expr[this.cursor + 3] == 101 && (ParseTools.isWhitespace(this.expr[this.cursor + 4]) || this.expr[this.cursor + 4] == 123);
        } else {
            return false;
        }
    }

    protected boolean tokenContinues() {
        if(this.cursor == this.end) {
            return false;
        } else if(this.expr[this.cursor] != 46 && this.expr[this.cursor] != 91) {
            if(ParseTools.isWhitespace(this.expr[this.cursor])) {
                int markCurrent = this.cursor;
                this.skipWhitespace();
                if(this.cursor != this.end && (this.expr[this.cursor] == 46 || this.expr[this.cursor] == 91)) {
                    return true;
                }

                this.cursor = markCurrent;
            }

            return false;
        } else {
            return true;
        }
    }

    protected void expectEOS() {
        this.skipWhitespace();
        if(this.cursor != this.end && this.expr[this.cursor] != 59) {
            switch(this.expr[this.cursor]) {
                case '!':
                    if(this.lookAhead() == 61) {
                        return;
                    }
                    break;
                case '&':
                    if(this.lookAhead() == 38) {
                        return;
                    }
                    break;
                case '*':
                case '+':
                case '-':
                case '/':
                    if(this.lookAhead() == 61) {
                        return;
                    }
                    break;
                case '<':
                case '>':
                    return;
                case '=':
                    switch(this.lookAhead()) {
                        case '*':
                        case '+':
                        case '-':
                        case '=':
                            return;
                        default:
                            throw new CompileException("expected end of statement but encountered: " + (this.cursor == this.end?"<end of stream>":Character.valueOf(this.expr[this.cursor])), this.expr, this.cursor);
                    }
                case '|':
                    if(this.lookAhead() == 124) {
                        return;
                    }
            }

            throw new CompileException("expected end of statement but encountered: " + (this.cursor == this.end?"<end of stream>":Character.valueOf(this.expr[this.cursor])), this.expr, this.cursor);
        }
    }

    protected boolean isNextIdentifier() {
        while(this.cursor != this.end && ParseTools.isWhitespace(this.expr[this.cursor])) {
            ++this.cursor;
        }

        return this.cursor != this.end && ParseTools.isIdentifierPart(this.expr[this.cursor]);
    }

    protected void captureToEOS() {
        for(; this.cursor != this.end; ++this.cursor) {
            switch(this.expr[this.cursor]) {
                case '\"':
                case '\'':
                    this.cursor = ParseTools.captureStringLiteral(this.expr[this.cursor], this.expr, this.cursor, this.end);
                    break;
                case '(':
                case '[':
                case '{':
                    if((this.cursor = ParseTools.balancedCaptureWithLineAccounting(this.expr, this.cursor, this.end, this.expr[this.cursor], this.pCtx)) >= this.end) {
                        return;
                    }
                    break;
                case ',':
                case ';':
                case '}':
                    return;
            }
        }

    }

    protected void captureToEOSorEOL() {
        while(this.cursor != this.end && this.expr[this.cursor] != 10 && this.expr[this.cursor] != 13 && this.expr[this.cursor] != 59) {
            ++this.cursor;
        }

    }

    protected void captureIdentifier() {
        boolean captured = false;
        if(this.cursor == this.end) {
            throw new CompileException("unexpected end of statement: EOF", this.expr, this.cursor);
        } else {
            while(this.cursor != this.end) {
                switch(this.expr[this.cursor]) {
                    case ';':
                        return;
                    default:
                        if(!ParseTools.isIdentifierPart(this.expr[this.cursor])) {
                            if(captured) {
                                return;
                            }

                            throw new CompileException("unexpected symbol (was expecting an identifier): " + this.expr[this.cursor], this.expr, this.cursor);
                        }

                        captured = true;
                        ++this.cursor;
                }
            }

        }
    }

    protected void captureToEOT() {
        this.skipWhitespace();

        do {
            switch(this.expr[this.cursor]) {
                case '\"':
                    this.cursor = ParseTools.captureStringLiteral('\"', this.expr, this.cursor, this.end);
                    break;
                case '&':
                case ',':
                case ';':
                case '=':
                case '|':
                    return;
                case '\'':
                    this.cursor = ParseTools.captureStringLiteral('\'', this.expr, this.cursor, this.end);
                    break;
                case '(':
                case '[':
                case '{':
                    if((this.cursor = ParseTools.balancedCaptureWithLineAccounting(this.expr, this.cursor, this.end, this.expr[this.cursor], this.pCtx)) == -1) {
                        throw new CompileException("unbalanced braces", this.expr, this.cursor);
                    }
                    break;
                case '.':
                    this.skipWhitespace();
                    break;
                default:
                    if(ParseTools.isWhitespace(this.expr[this.cursor])) {
                        this.skipWhitespace();
                        if(this.cursor >= this.end || this.expr[this.cursor] != 46) {
                            this.trimWhitespace();
                            return;
                        }

                        if(this.cursor != this.end) {
                            ++this.cursor;
                        }

                        this.skipWhitespace();
                    }
            }
        } while(++this.cursor < this.end);

    }

    protected boolean lastNonWhite(char c) {
        int i;
        for(i = this.cursor - 1; ParseTools.isWhitespace(this.expr[i]); --i) {
            ;
        }

        return c == this.expr[i];
    }

    protected int trimLeft(int pos) {
        if(pos > this.end) {
            pos = this.end;
        }

        while(pos > 0 && pos >= this.st && (ParseTools.isWhitespace(this.expr[pos - 1]) || this.expr[pos - 1] == 59)) {
            --pos;
        }

        return pos;
    }

    protected int trimRight(int pos) {
        while(pos != this.end && ParseTools.isWhitespace(this.expr[pos])) {
            ++pos;
        }

        return pos;
    }

    protected void skipWhitespace() {
        while(true) {
            label71: {
                label70:
                while(this.cursor != this.end) {
                    switch(this.expr[this.cursor]) {
                        case '\n':
                            ++this.line;
                            this.lastLineStart = this.cursor;
                        case '\r':
                            break label71;
                        case '/':
                            if(this.cursor + 1 != this.end) {
                                switch(this.expr[this.cursor + 1]) {
                                    case '*':
                                        int len = this.end - 1;

                                        int st;
                                        for(st = this.cursor++; this.cursor != len && (this.expr[this.cursor] != 42 || this.expr[this.cursor + 1] != 47); ++this.cursor) {
                                            ;
                                        }

                                        if(this.cursor != len) {
                                            this.cursor += 2;
                                        }

                                        int i = st;

                                        while(true) {
                                            if(i >= this.cursor) {
                                                continue label70;
                                            }

                                            this.expr[i] = 32;
                                            ++i;
                                        }
                                    case '/':
                                        ++this.cursor;

                                        while(this.cursor != this.end && this.expr[this.cursor] != 10) {
                                            ++this.cursor;
                                        }

                                        if(this.cursor != this.end) {
                                            ++this.cursor;
                                        }

                                        ++this.line;
                                        this.lastLineStart = this.cursor;
                                        continue;
                                    default:
                                        return;
                                }
                            }
                        default:
                            if(!ParseTools.isWhitespace(this.expr[this.cursor])) {
                                return;
                            }

                            ++this.cursor;
                    }
                }

                return;
            }

            ++this.cursor;
        }
    }

    protected void captureToNextTokenJunction() {
        while(this.cursor != this.end) {
            switch(this.expr[this.cursor]) {
                case '(':
                case '{':
                    return;
                case '/':
                    if(this.expr[this.cursor + 1] == 42) {
                        return;
                    }
                case '[':
                    this.cursor = ParseTools.balancedCaptureWithLineAccounting(this.expr, this.cursor, this.end, '[', this.pCtx) + 1;
                    break;
                default:
                    if(ParseTools.isWhitespace(this.expr[this.cursor])) {
                        return;
                    }

                    ++this.cursor;
            }
        }

    }

    protected void trimWhitespace() {
        while(this.cursor != 0 && ParseTools.isWhitespace(this.expr[this.cursor - 1])) {
            --this.cursor;
        }

    }

    protected void setExpression(String expression) {
        if(expression != null && expression.length() != 0) {
            WeakHashMap var2 = EX_PRECACHE;
            synchronized(EX_PRECACHE) {
                if((this.expr = (char[])EX_PRECACHE.get(expression)) != null) {
                    this.end = this.length = this.expr.length;
                } else {
                    for(this.end = this.length = (this.expr = expression.toCharArray()).length; this.length != 0 && ParseTools.isWhitespace(this.expr[this.length - 1]); --this.length) {
                        ;
                    }

                    char[] e = new char[this.length];

                    for(int i = 0; i != e.length; ++i) {
                        e[i] = this.expr[i];
                    }

                    EX_PRECACHE.put(expression, e);
                }
            }
        }

    }

    protected void setExpression(char[] expression) {
        for(this.end = this.length = (this.expr = expression).length; this.length != 0 && ParseTools.isWhitespace(this.expr[this.length - 1]); --this.length) {
            ;
        }

    }

    protected char lookToLast() {
        if(this.cursor == this.start) {
            return '\u0000';
        } else {
            int temp = this.cursor;

            while(temp != this.start) {
                --temp;
                if(!ParseTools.isWhitespace(this.expr[temp])) {
                    break;
                }
            }

            return this.expr[temp];
        }
    }

    protected char lookBehind() {
        return this.cursor == this.start?'\u0000':this.expr[this.cursor - 1];
    }

    protected char lookAhead() {
        return this.cursor + 1 != this.end?this.expr[this.cursor + 1]:'\u0000';
    }

    protected char lookAhead(int range) {
        return this.cursor + range >= this.end?'\u0000':this.expr[this.cursor + range];
    }

    protected boolean isNextIdentifierOrLiteral() {
        int tmp = this.cursor;
        if(tmp == this.end) {
            return false;
        } else {
            while(tmp != this.end && ParseTools.isWhitespace(this.expr[tmp])) {
                ++tmp;
            }

            if(tmp == this.end) {
                return false;
            } else {
                char n = this.expr[tmp];
                return ParseTools.isIdentifierPart(n) || ParseTools.isDigit(n) || n == 39 || n == 34;
            }
        }
    }

    public int incNextNonBlank() {
        ++this.cursor;
        return this.nextNonBlank();
    }

    public int nextNonBlank() {
        if(this.cursor + 1 >= this.end) {
            throw new CompileException("unexpected end of statement", this.expr, this.st);
        } else {
            int i;
            for(i = this.cursor; i != this.end && ParseTools.isWhitespace(this.expr[i]); ++i) {
                ;
            }

            return i;
        }
    }

    public void expectNextChar_IW(char c) {
        this.nextNonBlank();
        if(this.cursor == this.end) {
            throw new CompileException("unexpected end of statement", this.expr, this.st);
        } else if(this.expr[this.cursor] != c) {
            throw new CompileException("unexpected character (\'" + this.expr[this.cursor] + "\'); was expecting: " + c, this.expr, this.st);
        }
    }

    protected boolean isStatementNotManuallyTerminated() {
        if(this.cursor >= this.end) {
            return false;
        } else {
            int c;
            for(c = this.cursor; c != this.end && ParseTools.isWhitespace(this.expr[c]); ++c) {
                ;
            }

            return c == this.end || this.expr[c] != 59;
        }
    }

    protected ParserContext getParserContext() {
        if(parserContext == null || parserContext.get() == null) {
            this.newContext();
        }

        return (ParserContext)parserContext.get();
    }

    public static ParserContext getCurrentThreadParserContext() {
        return contextControl(3, (ParserContext)null, (AbstractParser)null);
    }

    public static void setCurrentThreadParserContext(ParserContext pCtx) {
        contextControl(0, pCtx, (AbstractParser)null);
    }

    public void newContext() {
        contextControl(0, new ParserContext(), this);
    }

    public void newContext(ParserContext pCtx) {
        contextControl(0, this.pCtx = pCtx, this);
    }

    public void removeContext() {
        contextControl(1, (ParserContext)null, this);
    }

    public static ParserContext contextControl(int operation, ParserContext pCtx, AbstractParser parser) {
        synchronized(Runtime.getRuntime()) {
            if(parserContext == null) {
                parserContext = new ThreadLocal();
            }

            switch(operation) {
                case 0:
                    pCtx.setRootParser(parser);
                    parserContext.set(pCtx);
                    return pCtx;
                case 1:
//                    parserContext.set((Object)null);
                    parserContext.set((ParserContext) null);
                    return null;
                case 3:
                    if(parserContext.get() == null) {
                        parserContext.set(new ParserContext(parser));
                    }
                case 2:
                    return (ParserContext)parserContext.get();
                default:
                    return null;
            }
        }
    }

    protected void addFatalError(String message) {
        this.pCtx.addError(new ErrorDetail(this.expr, this.st, true, message));
    }

    protected void addFatalError(String message, int start) {
        this.pCtx.addError(new ErrorDetail(this.expr, start, true, message));
    }

    public static void setLanguageLevel(int level) {
        OPERATORS.clear();
        OPERATORS.putAll(loadLanguageFeaturesByLevel(level));
    }

    public static HashMap<String, Integer> loadLanguageFeaturesByLevel(int languageLevel) {
        HashMap operatorsTable = new HashMap();
        switch(languageLevel) {
            case 6:
                operatorsTable.put("proto", Integer.valueOf(48));
            case 5:
                operatorsTable.put("if", Integer.valueOf(39));
                operatorsTable.put("else", Integer.valueOf(40));
                operatorsTable.put("?", Integer.valueOf(29));
                operatorsTable.put("switch", Integer.valueOf(44));
                operatorsTable.put("function", Integer.valueOf(100));
                operatorsTable.put("def", Integer.valueOf(100));
                operatorsTable.put("stacklang", Integer.valueOf(101));
            case 4:
                operatorsTable.put("=", Integer.valueOf(31));
                operatorsTable.put("var", Integer.valueOf(98));
                operatorsTable.put("+=", Integer.valueOf(52));
                operatorsTable.put("-=", Integer.valueOf(53));
                operatorsTable.put("/=", Integer.valueOf(55));
                operatorsTable.put("%=", Integer.valueOf(56));
            case 3:
                operatorsTable.put("foreach", Integer.valueOf(38));
                operatorsTable.put("while", Integer.valueOf(41));
                operatorsTable.put("until", Integer.valueOf(42));
                operatorsTable.put("for", Integer.valueOf(43));
                operatorsTable.put("do", Integer.valueOf(45));
            case 2:
                operatorsTable.put("return", Integer.valueOf(99));
                operatorsTable.put(";", Integer.valueOf(37));
            case 1:
                operatorsTable.put("+", Integer.valueOf(0));
                operatorsTable.put("-", Integer.valueOf(1));
                operatorsTable.put("*", Integer.valueOf(2));
                operatorsTable.put("**", Integer.valueOf(5));
                operatorsTable.put("/", Integer.valueOf(3));
                operatorsTable.put("%", Integer.valueOf(4));
                operatorsTable.put("==", Integer.valueOf(18));
                operatorsTable.put("!=", Integer.valueOf(19));
                operatorsTable.put(">", Integer.valueOf(15));
                operatorsTable.put(">=", Integer.valueOf(17));
                operatorsTable.put("<", Integer.valueOf(14));
                operatorsTable.put("<=", Integer.valueOf(16));
                operatorsTable.put("&&", Integer.valueOf(21));
                operatorsTable.put("and", Integer.valueOf(21));
                operatorsTable.put("||", Integer.valueOf(22));
                operatorsTable.put("or", Integer.valueOf(23));
                operatorsTable.put("~=", Integer.valueOf(24));
                operatorsTable.put("instanceof", Integer.valueOf(25));
                operatorsTable.put("is", Integer.valueOf(25));
                operatorsTable.put("contains", Integer.valueOf(26));
                operatorsTable.put("soundslike", Integer.valueOf(27));
                operatorsTable.put("strsim", Integer.valueOf(28));
                operatorsTable.put("convertable_to", Integer.valueOf(36));
                operatorsTable.put("isdef", Integer.valueOf(47));
                operatorsTable.put("#", Integer.valueOf(20));
                operatorsTable.put("&", Integer.valueOf(6));
                operatorsTable.put("|", Integer.valueOf(7));
                operatorsTable.put("^", Integer.valueOf(8));
                operatorsTable.put("<<", Integer.valueOf(10));
                operatorsTable.put("<<<", Integer.valueOf(12));
                operatorsTable.put(">>", Integer.valueOf(9));
                operatorsTable.put(">>>", Integer.valueOf(11));
                operatorsTable.put("new", Integer.valueOf(34));
                operatorsTable.put("in", Integer.valueOf(35));
                operatorsTable.put("with", Integer.valueOf(46));
                operatorsTable.put("assert", Integer.valueOf(97));
                operatorsTable.put("import", Integer.valueOf(96));
                operatorsTable.put("import_static", Integer.valueOf(95));
                operatorsTable.put("++", Integer.valueOf(50));
                operatorsTable.put("--", Integer.valueOf(51));
            case 0:
                operatorsTable.put(":", Integer.valueOf(30));
            default:
                return operatorsTable;
        }
    }

    public static void resetParserContext() {
        contextControl(1, (ParserContext)null, (AbstractParser)null);
    }

    protected static boolean isArithmeticOperator(int operator) {
        return operator != -1 && operator < 6;
    }

    protected int arithmeticFunctionReduction(int operator) {
        ASTNode tk;
        if((tk = this.nextToken()) != null) {
            int operator2;
            if(isArithmeticOperator(operator2 = tk.getOperator().intValue()) && Operator.PTABLE[operator2] > Operator.PTABLE[operator]) {
                this.stk.xswap();
                tk = this.nextToken();
                if(this.compileMode && !tk.isLiteral()) {
                    this.splitAccumulator.push(tk, new OperatorNode(Integer.valueOf(operator2), this.expr, this.st));
                    return -2;
                }

                operator = operator2;
                this.dStack.push(Integer.valueOf(operator2), tk.getReducedValue(this.ctx, this.ctx, this.variableFactory));

                label119:
                while(true) {
                    while((tk = this.nextToken()) == null || (operator2 = tk.getOperator().intValue()) == -1 || operator2 == 37 || Operator.PTABLE[operator2] <= Operator.PTABLE[operator]) {
                        if(tk == null || operator2 == -1 || operator2 == 37) {
                            if(this.dStack.size() > 1) {
                                this.dreduce();
                            }

                            if(this.stk.isReduceable()) {
                                this.stk.xswap();
                            }
                            break label119;
                        }

                        if(Operator.PTABLE[operator2] == Operator.PTABLE[operator]) {
                            if(!this.dStack.isEmpty()) {
                                this.dreduce();
                            } else {
                                while(this.stk.isReduceable()) {
                                    this.stk.xswap_op();
                                }
                            }

                            operator = operator2;
                            this.dStack.push(Integer.valueOf(operator2), this.nextToken().getReducedValue(this.ctx, this.ctx, this.variableFactory));
                        } else {
                            while(this.dStack.size() > 1) {
                                this.dreduce();
                            }

                            operator = tk.getOperator().intValue();

                            while(this.stk.size() != 1 && this.stk.peek2() instanceof Integer && (operator2 = ((Integer)this.stk.peek2()).intValue()) < Operator.PTABLE.length && Operator.PTABLE[operator2] >= Operator.PTABLE[operator]) {
                                this.stk.xswap_op();
                            }

                            if((tk = this.nextToken()) != null) {
                                switch(operator) {
                                    case 21:
                                        if(!this.stk.peekBoolean().booleanValue()) {
                                            return -1;
                                        }

                                        this.splitAccumulator.add(tk);
                                        return 21;
                                    case 22:
                                        if(this.stk.peekBoolean().booleanValue()) {
                                            return -1;
                                        }

                                        this.splitAccumulator.add(tk);
                                        return 22;
                                    default:
                                        this.stk.push(Integer.valueOf(operator), tk.getReducedValue(this.ctx, this.ctx, this.variableFactory));
                                }
                            }
                        }
                    }

                    if(this.dStack.isReduceable()) {
                        this.stk.copyx2(this.dStack);
                    }

                    operator = operator2;
                    this.dStack.push(Integer.valueOf(operator2), this.nextToken().getReducedValue(this.ctx, this.ctx, this.variableFactory));
                }
            } else {
                if(!tk.isOperator()) {
                    throw new CompileException("unexpected token: " + tk.getName(), this.expr, this.st);
                }

                this.reduce();
                this.splitAccumulator.push(tk);
            }
        }

        if(this.stk.isReduceable()) {
            while(true) {
                this.reduce();
                if(!this.stk.isReduceable()) {
                    break;
                }

                this.stk.xswap();
            }
        }

        return 0;
    }

    private void dreduce() {
        this.stk.copy2(this.dStack);
        this.stk.op();
    }

    protected void reduce() {
        try {
            Object v1;
            int operator;
            switch(operator = ((Integer)this.stk.pop()).intValue()) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                    this.stk.op(operator);
                    break;
                case 6:
                    this.stk.push(Integer.valueOf(asInt(this.stk.peek2()) & asInt(this.stk.pop2())));
                    break;
                case 7:
                    this.stk.push(Integer.valueOf(asInt(this.stk.peek2()) | asInt(this.stk.pop2())));
                    break;
                case 8:
                    this.stk.push(Integer.valueOf(asInt(this.stk.peek2()) ^ asInt(this.stk.pop2())));
                    break;
                case 9:
                    this.stk.push(Integer.valueOf(asInt(this.stk.peek2()) >> asInt(this.stk.pop2())));
                    break;
                case 10:
                    this.stk.push(Integer.valueOf(asInt(this.stk.peek2()) << asInt(this.stk.pop2())));
                    break;
                case 11:
                    this.stk.push(Integer.valueOf(asInt(this.stk.peek2()) >>> asInt(this.stk.pop2())));
                    break;
                case 12:
                    int e = asInt(this.stk.peek2());
                    if(e < 0) {
                        e *= -1;
                    }

                    this.stk.push(Integer.valueOf(e << asInt(this.stk.pop2())));
                case 13:
                case 20:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                default:
                    break;
                case 21:
                    v1 = this.stk.pop();
                    this.stk.push(Boolean.valueOf(((Boolean)this.stk.pop()).booleanValue() && ((Boolean)v1).booleanValue()));
                    break;
                case 22:
                    v1 = this.stk.pop();
                    this.stk.push(Boolean.valueOf(((Boolean)this.stk.pop()).booleanValue() || ((Boolean)v1).booleanValue()));
                    break;
                case 23:
                    v1 = this.stk.pop();
                    Object v2;
                    if(!PropertyTools.isEmpty(v2 = this.stk.pop()) || !PropertyTools.isEmpty(v1)) {
                        this.stk.clear();
                        this.stk.push(!PropertyTools.isEmpty(v2)?v2:v1);
                        return;
                    }

                    this.stk.push((Object)null);
                    break;
                case 24:
                    this.stk.push(Boolean.valueOf(Pattern.compile(String.valueOf(this.stk.pop())).matcher(String.valueOf(this.stk.pop())).matches()));
                    break;
                case 25:
                    this.stk.push(Boolean.valueOf(((Class)this.stk.pop()).isInstance(this.stk.pop())));
                    break;
                case 26:
                    this.stk.push(Boolean.valueOf(ParseTools.containsCheck(this.stk.peek2(), this.stk.pop2())));
                    break;
                case 27:
                    this.stk.push(Boolean.valueOf(Soundex.soundex(String.valueOf(this.stk.pop())).equals(Soundex.soundex(String.valueOf(this.stk.pop())))));
                    break;
                case 28:
                    this.stk.push(Float.valueOf(ParseTools.similarity(String.valueOf(this.stk.pop()), String.valueOf(this.stk.pop()))));
                    break;
                case 36:
                    this.stk.push(Boolean.valueOf(DataConversion.canConvert(this.stk.peek2().getClass(), (Class)this.stk.pop2())));
            }

        } catch (ClassCastException var5) {
            throw new CompileException("syntax error or incompatable types", this.expr, this.st, var5);
        } catch (ArithmeticException var6) {
            throw new CompileException("arithmetic error: " + var6.getMessage(), this.expr, this.st, var6);
        } catch (Exception var7) {
            throw new CompileException("failed to subEval expression", this.expr, this.st, var7);
        }
    }

    public int getCursor() {
        return this.cursor;
    }

    public char[] getExpression() {
        return this.expr;
    }

    private static int asInt(Object o) {
        return ((Integer)o).intValue();
    }

    public void setPCtx(ParserContext pCtx) {
        this.debugSymbols = (this.pCtx = pCtx).isDebugSymbols();
    }

    static {
        setupParser();
    }
}
