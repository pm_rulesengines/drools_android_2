package pl.me.drools;

/**
 * Created by Piotr1 on 2016-08-03.
 */
public class ocenaTestow {
    private double value;

    public ocenaTestow(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ocenaTestow{" +
                "value=" + value +
                '}';
    }
}
