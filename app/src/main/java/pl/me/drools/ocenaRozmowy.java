package pl.me.drools;

/**
 * Created by Piotr1 on 2016-08-03.
 */
public class ocenaRozmowy {
    private double value;

    public ocenaRozmowy(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ocenaRozmowy{" +
                "value=" + value +
                '}';
    }
}
