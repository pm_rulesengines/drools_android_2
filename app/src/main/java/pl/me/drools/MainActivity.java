package pl.me.drools;

import android.app.Activity;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends Activity {

    private static String PATH = Environment.getExternalStorageDirectory().getAbsolutePath()
            +"/drools/";
    private TextView textView;
    private static List<Long> memoryUsage = new LinkedList<>();
    private static List<Float> processorUsage = new LinkedList<>();
    private static boolean ifContinue = true;
    private Date start, end;
    private BufferedWriter bw;
    private RadioGroup radioGroup;
    private String selectedText;
    private KnowledgeBase kbase;
    private StatefulKnowledgeSession ksession;
    private boolean onlyTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textview);
        textView.setMovementMethod(new ScrollingMovementMethod());
        textView.setKeepScreenOn(true);
        radioGroup = (RadioGroup) findViewById(R.id.grupa);
        File DroolsBaseDir = new File(PATH);
        for (File model : DroolsBaseDir.listFiles()) {
            if (!model.getName().endsWith(".drl")) continue;
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(model.getName());
            radioGroup.addView(radioButton);
        }
    }

    public void go (View view) {
        try {
            int radioButtonID = radioGroup.getCheckedRadioButtonId();
            View radioButton = radioGroup.findViewById(radioButtonID);
            int idx = radioGroup.indexOfChild(radioButton);
            RadioButton r = (RadioButton)  radioGroup.getChildAt(idx);
            selectedText = r.getText().toString();

//            bw = new BufferedWriter(new FileWriter(Environment.getExternalStorageDirectory()
//                    .getAbsolutePath()+"/benchmark/drools.txt",true));
            StringBuilder sb = new StringBuilder();
            onlyTest = false;

            //measuring block
//            ifContinue = true;
//            new Monitor().execute();
//            start = new Date();
            //measuring block

            kbase = readKnowledgeBase(PATH + selectedText);
            ksession = kbase.newStatefulKnowledgeSession();

            //This section sets the initial values of required attributes (for sample model),
            //but it's commented since the initial values are set in the DRL file
            /*ocenaTestow ocenaTestow = new ocenaTestow(2.0);
            ocenaRozmowy ocenaRozmowy = new ocenaRozmowy(2.0);
            ocenaKwalifikacji ocenaKwalifikacji = new ocenaKwalifikacji(5.0);
            ksession.insert(ocenaKwalifikacji);
            ksession.insert(ocenaRozmowy);
            ksession.insert(ocenaTestow);*/

            ksession.fireAllRules();

//            end = new Date();
//            ifContinue = false;

//            System.out.println(ksession.getObjects().size());
//            System.out.println("---------------------------------");
//            for (Object o : ksession.getObjects()) {
//                System.out.println(o.toString());
//            }
            for (Object o : ksession.getObjects()) {
                sb.append(o.toString()).append("\n");
            }
            radioGroup.setVisibility(View.GONE);
            textView.setText(sb.toString());
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r1 = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r1.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void goOnlyRun(View view) {
        try {
            int radioButtonID = radioGroup.getCheckedRadioButtonId();
            View radioButton = radioGroup.findViewById(radioButtonID);
            int idx = radioGroup.indexOfChild(radioButton);
            RadioButton r = (RadioButton)  radioGroup.getChildAt(idx);
            selectedText = r.getText().toString();

            bw = new BufferedWriter(new FileWriter(Environment.getExternalStorageDirectory()
                    .getAbsolutePath()+"/benchmark/drools.txt",true));
            StringBuilder sb = new StringBuilder();

            onlyTest = true;

            if (kbase == null)
                kbase = readKnowledgeBase(PATH+selectedText);
            if (ksession == null)
                ksession = kbase.newStatefulKnowledgeSession();

            //measuring block
            ifContinue = true;
            new Monitor().execute();
            start = new Date();
            //measuring block

            ksession.fireAllRules();

            end = new Date();
            ifContinue = false;

//            System.out.println(ksession.getObjects().size());
//            System.out.println("---------------------------------");
//            for (Object o : ksession.getObjects()) {
//                System.out.println(o.toString());
//            }
            for (Object o : ksession.getObjects()) {
                sb.append(o.toString()).append("\n");
            }
            radioGroup.setVisibility(View.GONE);
            textView.setText(sb.toString());
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r2 = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r2.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private KnowledgeBase readKnowledgeBase(String fileName) throws Exception {

        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();

//        kbuilder.add(ResourceFactory.newClassPathResource("Pune.drl"), ResourceType.DRL);
        kbuilder.add(ResourceFactory.newFileResource(new File(fileName)), ResourceType.DRL);

        KnowledgeBuilderErrors errors = kbuilder.getErrors();

        if (errors.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (KnowledgeBuilderError error: errors) {
                System.err.println(error);
                sb.append(error).append("\n");
            }
            textView.setText(sb.toString());
            throw new IllegalArgumentException("Could not parse knowledge.");
        }

        KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
        kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());

        return kbase;
    }

    private static String printDateTime() {
        Calendar c = Calendar.getInstance();
        return String.format("%tY-%tm-%td %tH:%tM:%tS : ", c, c, c, c, c, c);
    }

    private float readUsage() {
        try {
            RandomAccessFile reader = new RandomAccessFile("/proc/stat", "r");
            String load = reader.readLine();

            String[] toks = load.split(" +");  // Split on one or more spaces

            long idle1 = Long.parseLong(toks[4]);
            long cpu1 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            try {
                Thread.sleep(360);
            } catch (Exception ignored) {}

            reader.seek(0);
            load = reader.readLine();
            reader.close();

            toks = load.split(" +");

            long idle2 = Long.parseLong(toks[4]);
            long cpu2 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            return (float)(cpu2 - cpu1) / ((cpu2 + idle2) - (cpu1 + idle1));

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    public class Monitor extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            while (ifContinue) {
                memoryUsage.add(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
                processorUsage.add(readUsage());
            }
            System.out.println("MONITOR: KONIEC");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Long [] mem = memoryUsage.toArray(new Long[memoryUsage.size()]);
            long minMem = mem[0];
            long maxMem = mem[0];
            for (long m : mem) {
                if (m < minMem) minMem = m;
                if (m > maxMem) maxMem = m;
            }

            Float [] proc = processorUsage.toArray(new Float[processorUsage.size()]);
            float minProc = proc[0];
            float maxProc = proc[0];
            for (float p : proc) {
                if (p < minProc) minProc = p;
                if (p > maxProc) maxProc = p;
            }
            try {
                bw.write(printDateTime());
                bw.write("("+selectedText+") ");
                bw.write("Mem diff: ");
                bw.write(String.valueOf( (maxMem - minMem) / 1024L));
                bw.write(" KB , Processor diff: ");
                bw.write(String.valueOf( (maxProc-minProc)*100));
                bw.write(" % , Time diff: ");
                bw.write(String.valueOf(end.getTime() - start.getTime()));
                bw.write(" ms "+(onlyTest ? "(tylko wyk)" : "")+" \n-------\n");
                bw.close();

            } catch (NullPointerException e) {
                System.err.println("Result file is not open!");
            }catch (IOException e) {
                e.printStackTrace();
            } finally {
                textView.setKeepScreenOn(false);
            }

        }
    }
}
