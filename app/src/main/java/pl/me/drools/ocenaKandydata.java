package pl.me.drools;

public class ocenaKandydata {
	private double value;

	public ocenaKandydata(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

    @Override
    public String toString() {
        return "ocenaKandydata{" +
                "value=" + value +
                '}';
    }
}
