# README #

This README contains specific information for Drools rules engine.

### Essential files ###

Before launching the app, you need to upload a .drl file with a model into phone/tablet memory. By default, the file must be placed in internal storage (not on the memory card) and must be placed inside "drools" directory. The sample model file is available in the "Downloads" section.